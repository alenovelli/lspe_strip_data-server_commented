#include <iostream>
#include <thread>
#include "include/strip/singleton.hpp"
#include "include/autobahn/base.hpp"
#include "include/strip/daq_pkt.hpp"
#include "include/strip/hk_stream.hpp"
#include <iostream>
#include <atomic>

#define MSGPACK_USE_BOOST 1
#include <boost/variant.hpp>
#include <msgpack.hpp>

class Receiver : public strip::autobahn::Base
{
public:
    Receiver(const boost::asio::ip::address &addr, uint16_t port) : strip::autobahn::Base(addr, port), _smjd(-1) {}

    void subscribe_pkt(const std::string &topic)
    {
        _pkt_sub = _session->subscribe(topic, std::bind(&Receiver::pkt_slot, std::ref(*this), std::placeholders::_1));
    }


private:
    boost::future<autobahn::wamp_subscription> _pkt_sub;
    void set_mjd(const double& mjd){
        if(_smjd < 0){
            _delta = 0;
            _smjd = mjd;
        }else{
            _delta = (mjd - _smjd) *86400.0;
        }
        _mjd = mjd;
    }

    void pkt_slot(const autobahn::wamp_event &e);

    std::atomic<double> _smjd;
    std::atomic<double> _mjd;
    std::atomic<double> _delta;


    std::mutex _m;
};

void Receiver::pkt_slot(const autobahn::wamp_event &e)
{
    try
    {
        //_mjd = e.kw_argument<double>("mjd");
        typedef msgpack::type::variant var;

        std::map<var, var> map;
        e.get_kw_arguments(map);

        if (map.find(std::string("bias")) != map.end())
        {
            auto &bias_map = map["bias"].as_multimap();

            if (bias_map.find("VD0_HK") != bias_map.end())
            {
                std::unique_lock<std::mutex> l(_m);
                double mdj = map["mjd"].as_double();

                std::cout << _delta << "," << (_mjd - mdj)*86400.0 << std::endl;
            }
            else
                set_mjd(map["mjd"].as_double());
        }
        else
            set_mjd(map["mjd"].as_double());
    }
    catch (const std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}



void th_go()
{
    strip::utils::ioService::service.run();
    std::cout << "----------------------------------ioService exiting" << std::endl;
}
constexpr int th_num = 1;
int main()
{
    std::vector<std::thread> th;

    try
    {
        strip::utils::parse_config("/etc/strip/config.json");
        //strip::utils::parse_config("/home/stefano/workspace/LSPE/data-server/my_config.json");
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }

    const strip::Config &conf = *strip::singletons.config;
    auto wamp_ip = strip::utils::resolve_or_fail(conf.wamp_router_ip);
    Receiver r(wamp_ip, conf.wamp_router_port);
    r.init(conf.wamp_realm).then([&r](boost::future<int> i) {
        r.subscribe_pkt("strip.fp.pol.R0");
    });

    for (auto i = 0; i < th_num; i++) //start async workers for sockets
        th.push_back(std::thread(th_go));

    for (auto &t : th)
        t.join();

    return 0;
}
