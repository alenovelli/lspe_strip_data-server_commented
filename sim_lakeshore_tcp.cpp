#include "include/strip/tcp.hpp"
#include "include/strip/utils.hpp"
#include "include/json/dispatcher.hpp"




#include <iostream>
int main(int argc, char *argv[])
{
std::cout << "start" << std::endl;
   try{

std::cout << "parse" << std::endl;
        strip::utils::parse_config("/etc/strip/config.json");

std::cout << "done" << std::endl;
        //strip::utils::parse_config("/home/stefano/workspace/LSPE/data-server/my_config.json");
    }catch(const std::exception& e){
        std::cerr << e.what() <<std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }

std::cout << "jsonControl" << std::endl;
  return  strip::JsonControl::init();
  return 0;
}