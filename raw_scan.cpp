#include "include/strip/daq_pkt.hpp"
#include <fstream>
#include <iostream>
#include <sys/time.h>

/*

time_t          tv_sec;
suseconds_t     tv_usec;

*/
int main(int argc, char* argv[]){
  char buff[strip::DAQ_PKT_LEN];


  std::string filename;
  while(std::cin >> filename){
    std::fstream fs;
    fs.open (filename, std::fstream::in);

    timeval ts;
    timeval delta;

    timerclear(&ts);

    size_t pkts = 0;
    while(fs){
        strip::DaqPkt p;
        fs.read(buff,strip::DAQ_PKT_LEN);
        if(!p.decode(buff,buff+strip::DAQ_PKT_LEN)){
            continue;
        }
        if(!timerisset(&ts)){
          ts = p.timestamp;
        }else{
          timersub(&p.timestamp,&ts,&delta);
          double diff = double(delta.tv_sec)*1000000 + double(delta.tv_usec);
          if(diff > 10000){
            std::cout << filename;
            std::cout << "," << pkts ;
            std::cout << "," << diff << std::endl; 
          }
          if(diff < 0){
            std::cout << filename;
            std::cout << "," << pkts ;
            std::cout << "," << diff << std::endl; 
          }
          ts = p.timestamp;
        }
      pkts++;
    }
  }
    return 0;
}