#include "include/strip/singleton.hpp"
#include "include/strip/utils.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <vector>

using namespace strip;

int main(int argc, char *argv[]) {
  try {
    strip::utils::parse_config("/etc/strip/config.json");
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    std::cerr << "EXIT due to a configuration error" << std::endl;
    return -1;
  }
  int err = strip::utils::init();
  if (err) {
    return err;
  }

  typedef std::map<msgpack::type::variant, msgpack::type::variant> mpmap;

  for (const auto &sw : singletons.cryo_probes) {
    /*std::string multi_str = "";
    if (sw.first == "0")
      multi_str = "ALL0";
    else if (sw.first == "1")
      multi_str = "ALL1";
    if (!multi_str.empty()) {
      try {
        mpmap cmd;
        cmd["command"] = multi_str;
        cmd["args"] = std::vector<msgpack::type::variant>();
        auto ret = singletons.cryo_units.at("Multiplexer")->send_command(cmd);
        auto &map_ret = ret.as_map();
        if (map_ret["status"].as_string() != "OK") {
          std::cout << "CryoStreamError: " << map_ret["error"].as_string()
                    << std::endl;
          continue;
        }
      } catch (const std::exception &e) {
        std::cout << "CryoStreamError: " << e.what() << std::endl;
        continue;
      }
    }*/
    for (const auto &ch : sw.second) {
      try {
        mpmap cmd;
        cmd["command"] = "SRDG?";
        cmd["args"] = std::vector<msgpack::type::variant>();
        cmd["args"].as_vector().push_back(ch.first);
        auto ret = singletons.cryo_units.at(ch.second->cryo_unit())->send_command(cmd);
        auto &map_ret = ret.as_map();
        if (map_ret["status"].as_string() == "OK") {
          map_ret["mjd"] = utils::Time::get_now_MJD();
          map_ret["id"] = ch.second->id();
          map_ret["calibrated"] = ch.second->calibrate(
              map_ret.at("ret").as_vector().front().as_double());
          std::shared_ptr<msgpack::type::variant> pkt_ptr(
              new msgpack::type::variant());
          *pkt_ptr = ret;
          //                    _cryo_pkt_sig(pkt_ptr);
          std::cout << sw.first << " " << ch.first << " " << ch.second->id()
                    << " : " << map_ret["calibrated"].as_double() << std::endl;
        } else {
          std::cout << "CryoStreamError: " << map_ret["error"].as_string()
                    << std::endl;
        }
      } catch (const std::exception &e) {
        std::cout << "CryoStreamError: " << e.what() << std::endl;
      }
    }
  }
}