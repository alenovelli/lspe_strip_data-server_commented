#include "../../include/db/hdf5_info.hpp"
#include "../../include/db/h5_info.hpp"
#include "../../odb/h5_info-odb.hxx"

#include <odb/transaction.hxx>

namespace strip {
const std::string DbH5info::class_name = "DbH5info";

DbH5info::DbH5info(const std::string& user, const std::string& password, const std::string& db_name, const std::string& host, uint16_t port){
    _db.reset(new odb::mysql::database (user,password,db_name,host,port));
}

void DbH5info::connect_h5info(signal_hdf5_file &sig){
    _conn = sig.connect(std::bind(&DbH5info::slot_h5info, std::ref(*this),std::placeholders::_1));
}

H5FileInfo DbH5info::slot_h5info(std::shared_ptr<const H5FileInfo> msg){
    H5FileInfo l = *msg;

    try{
    odb::transaction t (_db->begin ());
    if(l.db_id == 0){
        _db->persist(l);
    }else{
        _db->update(l);
    }
    t.commit();
    }catch(const odb::exception& e){
        utils::Log::log(utils::LogMessage::ERROR,class_name,e.what());
    }
    return l;
}

}
