#include "../../include/cryo/cryo_probe.hpp"
#include "../../include/strip/utils.hpp"
#include <math.h>

void CryoProbe::init()
{
    std::fstream fs;
    fs.open(_calibration_filename, std::fstream::in);
    constexpr size_t BUFF_SIZE = 512;
    char line[BUFF_SIZE];

    std::vector<double> x;
    std::vector<double> y;
    bool exp = false;
    while (fs)
    {
        double d0, d1;
        fs.getline(line, BUFF_SIZE);
        std::string str(line);
        if (str.find("Format") != std::string::npos)
        {
            size_t h = str.find(":") + 1;
            std::string format = str.substr(h, std::string::npos);
            int f = std::stoi(format);
            if (f == 4)
                exp = true;
            continue;
        }
        if (str.find("Point") == std::string::npos)
            continue;

        size_t h = str.find(":") + 1;
        size_t s = str.find(",");

        if (s != std::string::npos)
        {
            std::string v0 = str.substr(h, s - h);
            std::string v1 = str.substr(s + 1, std::string::npos);

            d0 = std::strtod(v0.c_str(), nullptr);
            d1 = std::strtod(v1.c_str(), nullptr);
        }
        else
        {
            str = str.substr(h);
            const char *c_str = str.c_str();
            char *ptr;
            d0 = std::strtod(c_str, &ptr);
            d1 = std::strtod(ptr, nullptr);
        }
        x.push_back(exp ? pow(10, d0) : d0);
        y.push_back(d1);
    }
    fs.close();
    if (x.size() != y.size() || x.empty())
    {
        std::cout << "data error " << std::endl;
        throw std::runtime_error("bad data in file " + _calibration_filename);
    }
    if (x.front() > x.back())
    { // pchip wants increasing x values
        std::reverse(x.begin(), x.end());
        std::reverse(y.begin(), y.end());
    }
    _interp.reset(new interpolator_t(std::move(x), std::move(y)));
}

double CryoProbe::calibrate(const double &val)
{
    using namespace strip::utils;

    if (_interp)
    {
        try
        {
            return _interp->operator()(val);
        }
        catch (const std::exception &e)
        {
            Log::log(LogMessage::Level::ERROR, "CryoProbe(" + _id + ")", e.what());
            return std::numeric_limits<double>::quiet_NaN();
        }
    }
    else
    {
        return std::numeric_limits<double>::quiet_NaN();
    }
}