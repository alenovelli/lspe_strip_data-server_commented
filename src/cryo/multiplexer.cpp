#include "../../include/cryo/multiplexer.hpp"
#include "../../include/strip/utils.hpp"
#include <iostream>

using boost::asio::ip::udp;
using namespace strip::utils;

const std::string Multiplexer::class_name = "Multiplexer";

Multiplexer::Multiplexer() : _socket(strip::utils::ioService::service)
{
}

CryoUnit::command_t Multiplexer::send_command(const command_t &c)
{
    if (_n_timeouts >= MAX_TIMEOUTS)
    { // we received MAX_TIMEOUTS consecutive timeouts, change connection
        Log::log(LogMessage::Level::INFO, class_name, "MULTIPLEXER: MAX_TIMEOUTS reached. Reopening connection");
        boost::system::error_code ec;
        _socket.close(ec);
        if (ec)
        {
            Log::log(LogMessage::Level::ERROR, class_name, "CLOSING CONNECTION: " + ec.message());
        }
        _socket = boost::asio::ip::udp::socket(ioService::service,
                                               udp::endpoint(boost::asio::ip::address::from_string("0.0.0.0"),
                                                             _remote_endpoint.port())); // bind to any address
    }
    command_t cmd = c;
    try
    {
        std::map<command_t, command_t> &map = cmd.as_map();
        std::string &command = map.at("command").as_string();
        if (command != "ALL0" && command != "ALL1")
            throw std::runtime_error("illegal command " + command);
        _socket.send_to(boost::asio::buffer(command), _remote_endpoint, 0);

        auto s = _socket.native_handle();
        fd_set set;
        struct timeval timeout;
        FD_ZERO(&set);   /* clear the set */
        FD_SET(s, &set); /* add our file descriptor to the set */
        timeout.tv_sec = WAIT_MILLIS / 1000;
        timeout.tv_usec = (WAIT_MILLIS % 1000) * 1000;

        int rv = select(s + 1, &set, NULL, NULL, &timeout);
        if (rv == -1)
        { // error occured
            throw std::runtime_error("select syscall error");
        }
        if (rv == 0)
        {
            _n_timeouts++;
            throw std::runtime_error("timeout error");
        }
        _n_timeouts = 0; // not timeout, so reset counter

        memset(_buffer, '\0', BUFFER_SIZE);
        socklen_t addr_len = _remote_endpoint.size();
        ssize_t recv_size = recvfrom(s, _buffer, BUFFER_SIZE, 0, _remote_endpoint.data(), &addr_len);
        if (recv_size == -1)
        { // error occured
            throw std::runtime_error(strerror(errno));
        }
        std::string msg(_buffer);

        std::vector<msgpack::type::variant> out_vec;
        

        if (command == "ALL0" && msg.find("STATUS=000000000000") != std::string::npos){
            map["status"] = "OK";
            out_vec.push_back("STATUS=000000000000");
        }
        if (command == "ALL1" && msg.find("STATUS=111111111111") != std::string::npos){
            map["status"] = "OK";
            out_vec.push_back("STATUS=111111111111");
        }
        map["ret"] = out_vec;
    }
    catch (const std::exception &e)
    {
        std::cerr << "MULTIPLEXER command error: " << e.what() << std::endl;
        if (!cmd.is_map())
            cmd = std::map<command_t, command_t>();

        cmd.as_map()["status"] = "ERROR";
        cmd.as_map()["error"] = std::string(e.what());

    }
    return cmd;
}

void Multiplexer::set_config(const boost::property_tree::ptree &pt)
{
    CryoUnit::set_config(pt); // set the commands
    _ip = pt.get<std::string>("connection.ip");
    _port = pt.get<uint16_t>("connection.port");
}
void Multiplexer::start(boost::system::error_code &ec)
{
    _remote_endpoint = udp::endpoint(resolve_or_fail(_ip), _port);
    _socket = udp::socket(ioService::service, udp::endpoint(boost::asio::ip::address::from_string("0.0.0.0"), _port));
}
void Multiplexer::stop(boost::system::error_code &ec)
{
    _socket.close(ec);
}