#include "../../include/cryo/lakeshore.hpp"
#include <iostream>
#include <iomanip>
#include <boost/algorithm/string.hpp>

Lakeshore::Lakeshore() {}

/* commands must be:
   {
      "command" : "*CMD",
      "args"    : ["arg1",num_arg]
   }
 */
Lakeshore::command_t Lakeshore::send_command(const command_t &c)
{
    std::unique_lock<std::mutex> l(_cmd_sync); // serialize concurrent command submissions
    boost::system::error_code ec;
    command_t cmd = c;
    std::map<command_t, command_t> &map_cmd = cmd.as_map();
    std::stringstream ss;
    bool ok = encode(cmd, ss);
    if (ok)
    {
        try
        {
            do_send(ss, ec);
            if (ec)
            {
                map_cmd["status"] = "ERROR_CONN_DEVICE";
                map_cmd["error"] = ec.message();
                return cmd;
            }
            if (!has_return(c))
            {
                map_cmd["status"] = "OK";
                map_cmd["ret"] = std::vector<msgpack::type::variant>(); // empty vector
                return cmd;
            }
            std::stringstream srec = do_receive(1000, ec);
            if (ec)
            {
                map_cmd["status"] = "ERROR_CONN_DEVICE";
                map_cmd["error"] = ec.message();
                return cmd;
            }
            ok = decode(srec, cmd);
            if (!ok)
            {
                map_cmd["status"] = "ERROR_BAD_RESPONSE";
                map_cmd["error"] = "device response was not legal";
            }
            else
            {
                map_cmd["status"] = "OK";
            }
        }
        catch (const std::exception &e)
        {
            map_cmd["status"] = "ERROR_DEVICE";
            map_cmd["error"] = std::string(e.what());
        }
    }
    else
    {
        std::cout << "LAKESHORE CMD ERROR" << std::endl;
        std::cout << "LAKESHORE ERROR; PARSED: " << ss.str() << std::endl;
        map_cmd["status"] = "ERROR_BAD_RQUEST";
        map_cmd["error"] = "not legal request";
    }
    return cmd;
}


bool Lakeshore::has_return(const command_t &cmd)
{
    try
    {
        const auto &map = cmd.as_map();
        const std::string &command = map.at("command").as_string();
        return _commands.at(command).second.size() != 0;
    }
    catch (const std::exception &e)
    {
        std::cout << "Lakeshore internal error: " << e.what() << std::endl;
    }
    return false;
}

bool Lakeshore::decode(std::stringstream &buff, command_t &cmd)
{
    std::vector<std::string> tokens;
    std::string text = buff.str();
    boost::algorithm::trim(text);

    boost::split(tokens, text, [](char c) { return c == ','; });

    try
    {
        auto &map = cmd.as_map();
        const std::string &command = map.at("command").as_string();
        std::vector<std::string> &out_format = _commands.at(command).second;
        if (out_format.size() != tokens.size())
        { // size mismatch
            std::cout << "expected " << out_format.size() << " args. got: " << tokens.size() << std::endl;
            return false;
        }
        std::vector<msgpack::type::variant> out_vec;
        for (size_t i = 0; i < out_format.size(); i++)
        {
            const std::string &rule = out_format[i];
            const std::string &token = tokens[i];

            if (rule[0] == 'a' ||
                rule[0] == 's' ||
                rule[0] == 'd')
            { //its a string
                out_vec.push_back(token);
            }
            else
            { //its a number
                double i = std::stod(token);
                out_vec.push_back(i);
            }
        }
        map["ret"] = out_vec;
        if (out_vec.size() == out_format.size())
        {
            map["status"] = "OK";
            return true;
        }
        else
        {
            map["status"] = "ERROR_INCOMPLETE";
            return false;
        }
    }
    catch (const std::exception &e)
    {
        if (cmd.is_map())
        {
            cmd.as_map()["status"] = "ERROR_DECODE";
            cmd.as_map()["error"] = std::string(e.what());
        }
        std::cerr << "LAKESHORE command error: " << e.what() << std::endl;
        return false;
    }
}
#include <cxxabi.h>
#include <typeinfo>

std::string demangle(const char *mangled)
{
    int status;
    std::unique_ptr<char[], void (*)(void *)> result(
        abi::__cxa_demangle(mangled, 0, 0, &status), std::free);
    return result.get() ? std::string(result.get()) : "error occurred";
}

class NumberOrFail : public boost::static_visitor<double>
{
public:
    double operator()(const double& t)const{
        return t;
    }
    double operator()(const int64_t& t)const{
        return t;
    }
    double operator()(const uint64_t& t)const{
        return t;
    }
    template<typename T>
    double operator()(const T& t)const {
        std::string msg = "bad integer: ";
        msg += demangle(typeid(t).name());
        throw std::runtime_error(msg);
    }
    
};

bool
Lakeshore::encode(command_t &cmd, std::stringstream &ss)
{
    try
    {
        auto &map = cmd.as_map();
        std::string &command = map.at("command").as_string();
        const std::vector<std::string> &in_format = _commands.at(command).first;
        const std::vector<msgpack::type::variant> &in_args = map.at("args").as_vector();
        if (in_format.size() != in_args.size())
        {
            std::cerr << "LAKESHORE n. inputs missmatch: " << in_format.size()
                      << " expected, " << in_args.size() << " provided";
            map["status"] = "ERROR_SYNTAX";
            return false;
        }
        ss << command << " ";
        for (size_t i = 0; i < in_format.size(); i++)
        {
            const std::string &in = in_format[i];
            if (in[0] == 'a' ||
                in[0] == 's' ||
                in[0] == 'd')
            { //string needed, control type and forward
                std::string s = in_args[i].as_string();
                if(s.find(term())!= std::string::npos){
                    //bad string,possible malevolous injection, just throw exception
                    throw std::runtime_error("bad argument");
                }
                if(s.find(";")!= std::string::npos){
                    //bad string,possible malevolous injection, just throw exception
                    throw std::runtime_error("bad argument");
                }
                ss << s;
            }
            else if (in[0] == '+' || in[0] == 'n')
            {
                // should be a number
                double val = boost::apply_visitor( NumberOrFail(), in_args[i]);
                ss << val;

                /* ALL as FLOAT
                if (in[0] == '+' && in[1] != '-')
                { // must apply + sign
                    ss << "+";
                }
                auto ppos = in.find('.');
                if (ppos != in.npos)
                { // floating point
                    double val = in_args[i].as_double();
                    int h_sign = 0;
                    if (in[0] == '+')
                        h_sign += 1;
                    if (in[1] == '-')
                        h_sign += 1;

                    ss << std::setw((in.size() - h_sign) - 1) << val;
                }
                else
                {
                    auto val = in_args[i].as_int64_t();
                    ss << val;
                }
                */
            }
            if (i != (in_format.size() - 1))
                ss << ",";
        }
        ss << term();
    }
    catch (const std::exception &e)
    {
        std::cerr << "LAKESHORE command error: " << e.what() << std::endl;
        if (cmd.is_map())
        {
            cmd.as_map()["status"] = "ERROR_SYNTAX";
            cmd.as_map()["error"] = std::string(e.what());
        }
        return false;
    }
    cmd.as_map()["status"] = "OK";
    return true;
}
