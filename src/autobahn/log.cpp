#include "../../include/autobahn/log.hpp"


typedef struct{
    std::string level;
    std::string emit_class;
    std::string user;
    std::string message;
    int64_t    timestamp;
} LogEntry;


namespace strip{
namespace autobahn{

const std::string Log::class_name = "autobahn::Log";


Log::Log(const boost::asio::ip::address& addr,uint16_t port) : Base(addr,port) {
    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&Log::slot_close, this));
}

void Log::connect_log(utils::signal_log &sig){
    _conn_log = sig.connect(std::bind(&Log::slot_log, std::ref(*this),std::placeholders::_1));
}

int Log::slot_close(){
    _conn_close.reset();
    leave();
    return  0;
}



void Log::slot_log(std::shared_ptr<const utils::LogMessage> msg){
    LogEntry e;
    switch (msg->level) {
    case utils::LogMessage::Level::INFO:
        e.level = "INFO";
        break;
    case utils::LogMessage::Level::DEBUG:
        e.level = "DEBUG";
        break;
    case utils::LogMessage::Level::WARNING:
        e.level = "WARNING";
        break;
    case utils::LogMessage::Level::ERROR:
        e.level = "ERROR";
        break;
    default:
        break;
    }

    e.emit_class = msg->emit_class;
    e.message = msg->message;
    e.user = msg->user;
    e.timestamp = msg->timestamp.time_since_epoch().count();

    std::tuple<int> arguments;
    publish("strip.log", arguments,e);
}


} //autobahn
} //strip


#include <msgpack.hpp>

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {

template <>
struct object_with_zone<LogEntry> {
    void operator()(msgpack::object::with_zone& o, LogEntry const& v) const {
        o.type = msgpack::type::MAP;
        uint32_t size = msgpack::checked_get_container_size(5);
        msgpack::object_kv* p = static_cast<msgpack::object_kv*>(o.zone.allocate_align(sizeof(msgpack::object_kv)*size, MSGPACK_ZONE_ALIGNOF(msgpack::object_kv)));
        o.via.map.ptr  = p;
        o.via.map.size = size;

        p[0].key = msgpack::object("level",o.zone);
        p[0].val = msgpack::object(v.level,o.zone);

        p[1].key = msgpack::object("emit_class",o.zone);
        p[1].val = msgpack::object(v.emit_class,o.zone);

        p[2].key = msgpack::object("user",o.zone);
        p[2].val = msgpack::object(v.user,o.zone);

        p[3].key = msgpack::object("message",o.zone);
        p[3].val = msgpack::object(v.message,o.zone);

        p[4].key = msgpack::object("timestamp",o.zone);
        p[4].val = msgpack::object(v.timestamp,o.zone);
    }
};

} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack
