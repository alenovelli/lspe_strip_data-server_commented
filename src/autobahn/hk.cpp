#include "../../include/autobahn/hk.hpp"
#include "../../include/strip/utils.hpp"

namespace strip{
namespace autobahn{

const std::string HkStream::class_name = "autobahn::HkStream";

HkStream::HkStream(const boost::asio::ip::address& addr,uint16_t port) :
    Base(addr,port)
{
    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&HkStream::slot_close, this));
}


void HkStream::connect_hk(signal_hk_pkt& sig){
    signal_hk_pkt::Connection conn = sig.connect(std::bind(&HkStream::slot_pkt, std::ref(*this),std::placeholders::_1));
    _conn_hk.push_back(conn);
}

int HkStream::slot_close(){
    _conn_close.reset();
    leave();
}

void HkStream::slot_pkt(std::shared_ptr<const msgpack::type::variant> ptr){
    msgpack::type::variant pkt = *ptr;
    std::string topic = "strip.fp";

    if(pkt.as_map().find("board") != pkt.as_map().end()){
        topic += ".board." + boost::get<std::string>(pkt.as_map().at("board"));
    }else if(pkt.as_map().find("pol") != pkt.as_map().end()){
        topic += ".pol." + boost::get<std::string>(pkt.as_map().at("pol"));
    }else{
        std::cerr << "invalid wamp packet" << std::endl;
        return;
    }

    std::tuple<int> arguments;
    publish(topic, arguments,pkt);
}



}
}
