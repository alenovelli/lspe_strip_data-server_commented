#include "../../include/strip/file_stream.hpp"
#include <time.h>
#include <libgen.h>
#include <iostream>
#include <sstream>
#include <experimental/filesystem>
#include <system_error>


namespace fs = std::experimental::filesystem;

namespace strip {


FileStream::FileStream() :
      ext(".bin"),
      class_name("FileStream")
{}

void FileStream::create(const std::string& str_time){
    _str_time = str_time;
    _conn_close = utils::UnixSignals::halt2_ops().connect(std::bind(&FileStream::slot_close, this));
    do_create();
}

int FileStream::slot_close(){
    _conn_timer_change_file.reset();
    _conn_timer_flush.reset();

    for(auto& p:_conn_pkt )
        p.reset();

    _conn_close.reset();
    std::unique_lock<std::recursive_mutex> l(_m);

    imp_flush();
    imp_close();

    std::cout << "STREAM " << class_name << " close"<<  std::endl;
    return 0;
}


void FileStream::do_create(){
    std::unique_lock<std::recursive_mutex> l(_m);

    char buffer[1024];
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    std:: string str_time_filename;

    auto len = strftime (buffer,1024,_str_time.c_str(),timeinfo);
    if(len == 0){
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"file path too long, using fallback");
        str_time_filename = "fallback";
    }else{
        str_time_filename = buffer;
    }
    std::string file_path = _base_path+str_time_filename;
    len = file_path.size()+1;
    char s1[len], s2[len];

    file_path.copy(s1,len);
    file_path.copy(s2,len);

    s1[len-1] = '\0';
    s2[len-1] = '\0';

    _fullpath = dirname(s1);
    _filename = basename(s2);

    try{
        fs::create_directories(_fullpath);

        file_path = _fullpath +"/"+ _filename + ext;

        if(fs::exists(fs::status(file_path))){
            auto i = 0;
            std::string path;
            std::string new_filename;
            do{
                std::stringstream ss;
                ss.width(4);
                ss.fill('0');
                ss << i++;
                new_filename = _filename + "_" + ss.str();
                file_path = _fullpath +"/"+ new_filename + ext;
            }while(fs::exists(fs::status(file_path)));
            _filename = new_filename;
        }
    }catch(const std::exception& e){
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,e.what());
    }

    _filename += ext;


    if(! imp_open(file_path)){
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"cannot open: "+file_path);
    }else{
        utils::Log::log(utils::LogMessage::Level::INFO,class_name,"opened: "+file_path);
    }


}


std::string FileStream::relative_path(){
    std::string path = _fullpath + "/" + _filename;
        std::string relative;
    auto p = path.find(_base_path);
    if(p != 0){
        utils::Log::log(utils::LogMessage::Level::ERROR,"FileStream","cannot substring the file path, using full path:" + path);
        relative = path;
    }else{
        relative = path.substr( _base_path.size());
    }
    return relative;
}

void FileStream::connect_pkt(signal_pkt& sig){
    signal_pkt::Connection conn = sig.connect(std::bind(&FileStream::slot_add, std::ref(*this),std::placeholders::_1));
    _conn_pkt.push_back(conn);
}

void FileStream::connect_hk(signal_hk_pkt& sig){
    signal_hk_pkt::Connection conn = sig.connect(std::bind(&FileStream::slot_hk, std::ref(*this),std::placeholders::_1));
    _conn_hk.push_back(conn);
}

void FileStream::connect_pol_flag(signal_pol_flag &sig){
    signal_pol_flag::Connection conn = sig.connect(std::bind(&FileStream::slot_pol_flag, std::ref(*this),std::placeholders::_1));
    _conn_pol_flag.push_back(conn);
}

void FileStream::connect_tel_flag(signal_tel_flag &sig){
    signal_tel_flag::Connection conn = sig.connect(std::bind(&FileStream::slot_tel_flag, std::ref(*this),std::placeholders::_1));
    _conn_tel_flag.push_back(conn);
}

void FileStream::connect_tag(signal_tag &sig){
    _conn_tag = sig.connect(std::bind(&FileStream::slot_tag, std::ref(*this),std::placeholders::_1));
}

void FileStream::connect_flush(signal_timer& sig){
    _conn_timer_flush = sig.connect(std::bind(&FileStream::slot_flush, this));
}

void FileStream::connect_change_file(signal_timer& sig){
    _conn_timer_change_file = sig.connect(std::bind(&FileStream::slot_change_file, this));
}

void FileStream::slot_flush(){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_flush();
}

void FileStream::slot_add(std::shared_ptr<const DaqUdpPkt> pkt){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_add(pkt);
}
void FileStream::slot_hk(std::shared_ptr<const msgpack::type::variant> pkt){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_hk(pkt);
}
void FileStream::slot_pol_flag(std::shared_ptr<const PolarimeterFlag> f){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_pol_flag(f);
}

void FileStream::slot_tel_flag(std::shared_ptr<const TelescopeFlag> f){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_tel_flag(f);
}

void FileStream::slot_tag(std::shared_ptr<const DbTag> tag){
    std::unique_lock<std::recursive_mutex> l(_m);
    imp_tag(tag);
}

void FileStream::slot_change_file(){
   std::unique_lock<std::recursive_mutex> l(_m);
   imp_flush();
   imp_close();
   do_create();
}

void FileStream::base_path(const std::string& path){
    if(path.back() == '/')
        _base_path = path;
    else
        _base_path = path + "/";
}

}
