#include "../../include/strip/hk_stream.hpp"
#include "../../include/strip/singleton.hpp"
namespace strip
{
    HkStream::HkStream(std::string board) : _hk_pkt_sig(singletons.workers_pool),
                                            _board(board)
    {
    }

    void HkStream::send_sig(SloCommand &req_hk)
    {
        typedef std::map<msgpack::type::variant, msgpack::type::variant> mpmap;

        const std::map<uint8_t, std::list<addr_item>::const_iterator> *address_map = nullptr;
        const Config &conf = *singletons.config;

        if (req_hk.res != SloCommand::Result::OK)
        {
            std::cerr << "HK request returned an error" << std::endl;
            return;
        }

        std::string table;
        uint8_t idx = 0;
        if (req_hk.pol_no == DaqPkt::HK_TABLE_BOARD)
        {
            if (req_hk.type == SloCommand::Type::BIAS)
            {
                table = "bias";
                address_map = &conf.map_bias_sys_u8;
                idx = conf.map_bias_sys_str.at(req_hk.base_addr)->address;
            }
            else if (req_hk.type == SloCommand::Type::DAQ)
            {
                table = "daq";
                address_map = &conf.map_daq_sys_u8;
                idx = conf.map_daq_sys_str.at(req_hk.base_addr)->address;
            }
            else
                return; //no bias nor daq
        }
        else
        {
            if (req_hk.type == SloCommand::Type::BIAS)
            {
                table = "bias";
                address_map = &conf.map_bias_pol_u8;
                idx = conf.map_bias_pol_str.at(req_hk.base_addr)->address;
            }
            else if (req_hk.type == SloCommand::Type::DAQ)
            {
                table = "daq";
                address_map = &conf.map_daq_pol_u8;
                idx = conf.map_daq_pol_str.at(req_hk.base_addr)->address;
            }
            else
                return; //no bias nor da
        }

        std::shared_ptr<msgpack::type::variant> pkt_ptr(new msgpack::type::variant());
        *pkt_ptr = mpmap();
        mpmap &pkt = pkt_ptr->as_map();
        if (req_hk.pol_no == DaqPkt::HK_TABLE_BOARD)
        {
            pkt["board"] = _board;
        }
        else
        {
            pkt["pol"] = conf.daq_name.at(_board)->pols[req_hk.pol_no];
        }
        pkt["mjd"] = utils::Time::get_unix_MJD(std::chrono::duration_cast<std::chrono::nanoseconds>(req_hk.timestamp.time_since_epoch()).count());
        pkt[table] = mpmap();
        mpmap &hk_map = pkt[table].as_map();

        for (auto &val : req_hk.data)
        {
            if (address_map->find(idx) == address_map->end())
            {
                idx++;
                continue;
            }
            const addr_item &item = *address_map->at(idx);
            if (item.type == item.i16)
            {
                int16_t *vp = reinterpret_cast<int16_t *>(&val);
                hk_map[item.name] = int64_t(*vp);
            }
            else
            {
                uint16_t *vp = reinterpret_cast<uint16_t *>(&val);
                hk_map[item.name] = uint64_t(*vp);
            }
            idx++;
        }
        _hk_pkt_sig(pkt_ptr);
    }

    void HkStream::read_hk(std::chrono::system_clock::time_point ts,uint16_t &value, size_t attempts)
    {
        if (attempts == 0)
            return;

        SloCommand req;
        req.board_name = _board;
        req.pol_no = 15; //BOARD
        req.method = SloCommand::Method::GET;
        req.type = SloCommand::Type::BIAS;
        req.base_addr = "HK_SCAN";
        req.data = {0};

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        singletons.slo_boards.at(_board)->send_command(req, 1000, false);
        value = req.data[0];

        if (req.res != SloCommand::Result::OK)
        {
            std::cerr << "HK request returned an error" << std::endl;
            return read_hk(ts,value, attempts - 1);
        }
        if (value != 0x5A00)
        {
            std::cerr << "HK not ready ("<< value <<"), retry in 100ms" << std::endl;
            return read_hk(ts,value, attempts - 1);
        }
        

        const std::vector<std::string> &pols = singletons.config->daq_name.at(_board)->pols;
        for (uint8_t i = 0; i < pols.size(); i++)
        {
            SloCommand req_hk;
            req_hk.board_name = _board;
            req_hk.pol_no = i;
            req_hk.method = SloCommand::Method::GET;
            req_hk.type = SloCommand::Type::BIAS;
            req_hk.base_addr = "VD0_HK";
            req_hk.data.assign(36, 0); //all the HK registers

            singletons.slo_boards.at(_board)->send_command(req_hk, 1000, false);
            req_hk.timestamp = ts; // the correct timestamp is the one from the latest HK_SCAN

            send_sig(req_hk);
        }
    }

    void HkStream::slot_collect_hk()
    {
        SloCommand req;
        req.board_name = _board;
        req.pol_no = 15; //BOARD
        req.method = SloCommand::Method::SET;
        req.type = SloCommand::Type::BIAS;
        req.base_addr = "HK_SCAN";
        req.data = {0x5AFF}; // 23295

        singletons.slo_boards.at(_board)->send_command(req, 1000, false);

        if (req.res != SloCommand::Result::OK)
        {
            std::cerr << "HK request returned an error" << std::endl;
            return;
        }

        std::chrono::system_clock::time_point hk_ts = req.timestamp;

        std::this_thread::sleep_for(std::chrono::milliseconds(100)); //200 ms should be fine

        uint16_t val;
        read_hk(hk_ts,val,8);
    }

    void HkStream::slot_collect_set()
    {
        const std::vector<std::string> &pols = singletons.config->daq_name.at(_board)->pols;
        for (uint8_t i = 0; i < pols.size(); i++)
        {
            SloCommand req_hk;
            req_hk.board_name = _board;
            req_hk.pol_no = i;
            req_hk.method = SloCommand::Method::GET;
            req_hk.type = SloCommand::Type::BIAS;
            req_hk.base_addr = "VD0_SET";
            req_hk.data.assign(80, 0); //from VD0_SET to POL_PWR registers

            singletons.slo_boards.at(_board)->send_command(req_hk, 1000, false);
            send_sig(req_hk);

            req_hk.base_addr = "MEAS_ID";
            req_hk.data.assign(124, 0); //from MEAS_ID to IG4A_SUM registers

            singletons.slo_boards.at(_board)->send_command(req_hk, 1000, false);
            send_sig(req_hk);

            req_hk.type = SloCommand::Type::DAQ;
            req_hk.base_addr = "DET0_BIAS";
            req_hk.data.assign(15, 0); //all daq registers execpt RFP_FLAG

            singletons.slo_boards.at(_board)->send_command(req_hk, 1000, false);
            send_sig(req_hk);
        }
        SloCommand req_hk;
        req_hk.board_name = _board;
        req_hk.pol_no = DaqPkt::HK_TABLE_BOARD;
        req_hk.method = SloCommand::Method::GET;
        req_hk.type = SloCommand::Type::BIAS;
        req_hk.base_addr = "START_PWR";
        req_hk.data.assign(235, 0); //from VD0_SET to POL_PWR registers

        singletons.slo_boards.at(_board)->send_command(req_hk, 1000, false);
        send_sig(req_hk);

        req_hk.type = SloCommand::Type::DAQ;
        req_hk.base_addr = "START_PWR";
        req_hk.data.assign(128, 0); //from VD0_SET to POL_PWR registers
        send_sig(req_hk);
    }

    void HkStream::connect_hk(signal_timer &sig)
    {
        _conn_timer_hk = sig.connect(std::bind(&HkStream::slot_collect_hk, this));
    }

    void HkStream::connect_set(signal_timer &sig)
    {
        _conn_timer_set = sig.connect(std::bind(&HkStream::slot_collect_set, this));
    }
} // namespace strip
