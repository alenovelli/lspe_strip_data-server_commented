#include "../../include/strip/timer.hpp"
#include "../../include/strip/singleton.hpp"

namespace strip {

Timer::Timer() :
    _timer(strip::utils::ioService::service),
    _sig(singletons.workers_pool),
    _ms(0),
    _go(true) {
    _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&Timer::slot_close, this));
}

void Timer::start(uint64_t mills){
    _ms = mills;
    _timer.expires_from_now(boost::posix_time::millisec(_ms));
    _timer.async_wait(std::bind(&Timer::emit_signal, std::ref(*this),std::placeholders::_1));
    _go = true;
}

void Timer::force_timeout(){
    stop();
    _sig();
    if(_ms > 0)
        start(_ms);
}

void Timer::stop(){
    _go = false;
    _timer.cancel();
}

void Timer::emit_signal(const boost::system::error_code& e){
    if(e == boost::asio::error::operation_aborted){
        return;
    }
    if(_go){
        _sig();
        _timer.expires_from_now(boost::posix_time::millisec(_ms));
        _timer.async_wait(std::bind(&Timer::emit_signal, std::ref(*this),std::placeholders::_1));
    }
}

int Timer::slot_close(){
    _sig.disconnect_all_slots();
    stop();
    return 0;
}

}
