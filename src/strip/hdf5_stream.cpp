#include "../../include/strip/hdf5_stream.hpp"
#include "../../include/strip/singleton.hpp"
#include <chrono>
#include <tuple>
#include <algorithm>
#include <sstream>

namespace strip {

const char* H5Stream::HK::field_names[H5Stream::HK::num_fields] = {
            "m_jd",
            "value"
        };

const size_t H5Stream::HK::dst_offset[H5Stream::HK::num_fields] = {
    HOFFSET( H5Stream::HK_data, m_jd ),
    HOFFSET( H5Stream::HK_data, u_val )
};

const size_t H5Stream::HK::dst_sizes[H5Stream::HK::num_fields] = {
    sizeof( H5Stream::HK_data::m_jd),
    sizeof( H5Stream::HK_data::u_val)
};

const char* H5Stream::Cryo::field_names[H5Stream::Cryo::num_fields] = {
            "m_jd",
            "raw",
            "calibrated"
        };

const size_t H5Stream::Cryo::dst_offset[H5Stream::Cryo::num_fields] = {
    HOFFSET( H5Stream::Cryo_data, m_jd ),
    HOFFSET( H5Stream::Cryo_data, raw ),
    HOFFSET( H5Stream::Cryo_data, cal )
};

const size_t H5Stream::Cryo::dst_sizes[H5Stream::Cryo::num_fields] = {
    sizeof( H5Stream::Cryo_data::m_jd),
    sizeof( H5Stream::Cryo_data::raw),
    sizeof( H5Stream::Cryo_data::cal)
};

const char* H5Stream::Polarimeter::field_names[H5Stream::Polarimeter::num_fields] = {
            "m_jd",
            "DEMQ1",
            "PWRQ1",
            "DEMU1",
            "PWRU1",
            "DEMU2",
            "PWRU2",
            "DEMQ2",
            "PWRQ2",
            "flag"
        };

const size_t H5Stream::Polarimeter::dst_offset[H5Stream::Polarimeter::num_fields] = {
    HOFFSET( H5Stream::Pol_data, m_jd ),
    HOFFSET( H5Stream::Pol_data, dem_q1 ),
    HOFFSET( H5Stream::Pol_data, pwr_q1 ),
    HOFFSET( H5Stream::Pol_data, dem_u1 ),
    HOFFSET( H5Stream::Pol_data, pwr_u1 ),
    HOFFSET( H5Stream::Pol_data, dem_u2 ),
    HOFFSET( H5Stream::Pol_data, pwr_u2 ),
    HOFFSET( H5Stream::Pol_data, dem_q2 ),
    HOFFSET( H5Stream::Pol_data, pwr_q2 ),
    HOFFSET( H5Stream::Pol_data, flags ),
};

const size_t H5Stream::Polarimeter::dst_sizes[H5Stream::Polarimeter::num_fields] = {
    sizeof( H5Stream::Pol_data::m_jd),
    sizeof( H5Stream::Pol_data::dem_q1),
    sizeof( H5Stream::Pol_data::pwr_q1),
    sizeof( H5Stream::Pol_data::dem_u1),
    sizeof( H5Stream::Pol_data::pwr_u1),
    sizeof( H5Stream::Pol_data::dem_u2),
    sizeof( H5Stream::Pol_data::pwr_u2),
    sizeof( H5Stream::Pol_data::dem_q2),
    sizeof( H5Stream::Pol_data::pwr_q2),
    sizeof( H5Stream::Pol_data::flags)
};

const char* H5Stream::TimeCorrelation::field_names[H5Stream::TimeCorrelation::num_fields] = {
            "m_jd",
            "unix_time_s",
            "unix_time_us",
            "PHB",
        };

const size_t H5Stream::TimeCorrelation::dst_sizes[H5Stream::TimeCorrelation::num_fields] = {
    sizeof( H5Stream::Time_corr_data::m_jd),
    sizeof( H5Stream::Time_corr_data::unix_time_s),
    sizeof( H5Stream::Time_corr_data::unix_time_us),
    sizeof( H5Stream::Time_corr_data::PHB)
};

const size_t H5Stream::TimeCorrelation::dst_offset[H5Stream::TimeCorrelation::num_fields] = {
    HOFFSET( H5Stream::Time_corr_data, m_jd ),
    HOFFSET( H5Stream::Time_corr_data, unix_time_s),
    HOFFSET( H5Stream::Time_corr_data, unix_time_us),
    HOFFSET( H5Stream::Time_corr_data, PHB )
};

const char* H5Stream::Telescope::field_names[H5Stream::Telescope::num_fields] = {
            "m_jd",
            "alt",
            "azi",
            "bor",
            "flags"
        };

const size_t H5Stream::Telescope::dst_sizes[H5Stream::Telescope::num_fields] = {
    sizeof( H5Stream::Tel_data::m_jd),
    sizeof( H5Stream::Tel_data::alt),
    sizeof( H5Stream::Tel_data::azi),
    sizeof( H5Stream::Tel_data::bor),
    sizeof( H5Stream::Tel_data::flags)
};

const size_t H5Stream::Telescope::dst_offset[H5Stream::Telescope::num_fields] = {
    HOFFSET( H5Stream::Tel_data, m_jd ),
    HOFFSET( H5Stream::Tel_data, alt ),
    HOFFSET( H5Stream::Tel_data, azi ),
    HOFFSET( H5Stream::Tel_data, bor ),
    HOFFSET( H5Stream::Tel_data, flags )
};

const char* H5Stream::TagTable::field_names[H5Stream::TagTable::num_fields] = {
            "id",
            "mjd_start",
            "mjd_end",
            "tag",
            "start_comment",
            "end_comment"
        };

const size_t H5Stream::TagTable::dst_sizes[H5Stream::TagTable::num_fields] = {
    sizeof( H5Stream::Tag_data::id),
    sizeof( H5Stream::Tag_data::m_jd_start),
    sizeof( H5Stream::Tag_data::m_jd_end),
    sizeof( H5Stream::Tag_data::tag),
    sizeof( H5Stream::Tag_data::start_comment),
    sizeof( H5Stream::Tag_data::end_comment)
};

const size_t H5Stream::TagTable::dst_offset[H5Stream::TagTable::num_fields] = {
    HOFFSET( H5Stream::Tag_data, id ),
    HOFFSET( H5Stream::Tag_data, m_jd_start ),
    HOFFSET( H5Stream::Tag_data, m_jd_end ),
    HOFFSET( H5Stream::Tag_data, tag ),
    HOFFSET( H5Stream::Tag_data, start_comment ),
    HOFFSET( H5Stream::Tag_data, end_comment )
};

const char* H5Stream::LogTable::field_names[H5Stream::LogTable::num_fields] = {
            "level",
            "m_jd",
            "emit_class",
            "user",
            "message"
        };

const size_t H5Stream::LogTable::dst_sizes[H5Stream::LogTable::num_fields] = {
    sizeof( H5Stream::Log_data::level),
    sizeof( H5Stream::Log_data::m_jd),
    sizeof( H5Stream::Log_data::emit_class),
    sizeof( H5Stream::Log_data::user),
    sizeof( H5Stream::Log_data::message)
};

const size_t H5Stream::LogTable::dst_offset[H5Stream::LogTable::num_fields] = {
    HOFFSET( H5Stream::Log_data, level ),
    HOFFSET( H5Stream::Log_data, m_jd ),
    HOFFSET( H5Stream::Log_data, emit_class ),
    HOFFSET( H5Stream::Log_data, user ),
    HOFFSET( H5Stream::Log_data, message )
};
/////////////////////////

const char* H5Stream::CommandTable::field_names[H5Stream::CommandTable::num_fields] = {
      "board_name",
      "pol_no",
      "method",
      "type",
      "base_addr",
      "message",
      "data",
      "user",
      "counter",
      "req_ack",
      "res",
      "m_jd"
        };

const size_t H5Stream::CommandTable::dst_sizes[H5Stream::CommandTable::num_fields] = {
    sizeof( H5Stream::Command_data::board_name),
    sizeof( H5Stream::Command_data::pol_no),
    sizeof( H5Stream::Command_data::method),
    sizeof( H5Stream::Command_data::type),
    sizeof( H5Stream::Command_data::base_addr),
    sizeof( H5Stream::Command_data::message),
    sizeof( H5Stream::Command_data::data),
    sizeof( H5Stream::Command_data::user),
    sizeof( H5Stream::Command_data::counter),
    sizeof( H5Stream::Command_data::req_ack),
    sizeof( H5Stream::Command_data::res),
    sizeof( H5Stream::Command_data::m_jd)
};

const size_t H5Stream::CommandTable::dst_offset[H5Stream::CommandTable::num_fields] = {
    HOFFSET( H5Stream::Command_data, board_name ),
    HOFFSET( H5Stream::Command_data, pol_no ),
    HOFFSET( H5Stream::Command_data, method ),
    HOFFSET( H5Stream::Command_data, type ),
    HOFFSET( H5Stream::Command_data, base_addr ),
    HOFFSET( H5Stream::Command_data, message ),
    HOFFSET( H5Stream::Command_data, data ),
    HOFFSET( H5Stream::Command_data, user ),
    HOFFSET( H5Stream::Command_data, counter ),
    HOFFSET( H5Stream::Command_data, req_ack ),
    HOFFSET( H5Stream::Command_data, res ),
    HOFFSET( H5Stream::Command_data, m_jd )
};

H5Stream::H5Stream() : _sig_file(singletons.workers_pool){
    /*

    char     board_id;
    uint8_t  hk_src;
    uint8_t  table_id;
    uint8_t  addr;

    */

    ext = ".h5";
    class_name = "H5Stream";

    auto populate_hk = [this](const std::string& board,const std::string& pol,const std::string& table_name,std::list<addr_item>& table){
        for(const addr_item& it : table){
            auto& hk_elem = _hk[board][pol][table_name][it.name];

            hk_elem.table_name = it.name;

            hk_elem.field_type[0] = H5T_NATIVE_DOUBLE;
            hk_elem.type = it.type;
            if(it.type == addr_item::Type::u16)
                hk_elem.field_type[1] = H5T_NATIVE_UINT16;
            else
                hk_elem.field_type[1] = H5T_NATIVE_INT16;
        }
    };

    for(const auto& daq : singletons.config->daq_id){
        char id = daq.second->id;
        std::string board_name = daq.second->name;
        for(uint8_t pol=0; pol<daq.second->pols.size(); pol++){
            _pols[id].push_back(Polarimeter());

            Polarimeter& p = _pols[id].back();
            p.group_name = "POL_"+daq.second->pols[pol];

            p.board_name = daq.second->name;
            p.pol_name   = daq.second->pols[pol];




            populate_hk(board_name,daq.second->pols[pol],"bias",singletons.config->address_bias_pol);
            populate_hk(board_name,daq.second->pols[pol],"daq" ,singletons.config->address_daq_pol);
        }

        populate_hk(board_name,"SYS","bias",singletons.config->address_bias_sys);
        populate_hk(board_name,"SYS","daq", singletons.config->address_daq_sys);
    }


}



void H5Stream::imp_flush(){
/*    using namespace std::chrono; //DBG
    high_resolution_clock::time_point t0 = high_resolution_clock::now(); //DBG
*/
    herr_t err = H5TBappend_records(_tel.gid,
                                    _tel.table_name.c_str(),
                                    _tel.data.size(),
                                    _tel.dst_size,
                                    _tel.dst_offset,
                                    _tel.dst_sizes,
                                    _tel.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records TELESCOPE returned:"+std::to_string(err));

    _tel.data.clear();

    err = H5TBappend_records(_time_corr.gid,
                                    _time_corr.table_name.c_str(),
                                    _time_corr.data.size(),
                                    _time_corr.dst_size,
                                    _time_corr.dst_offset,
                                    _time_corr.dst_sizes,
                                    _time_corr.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records TIME_CORRELATION returned:"+std::to_string(err));

    _time_corr.data.clear();

    err = H5TBappend_records(_tag_table.gid,
                                    _tag_table.table_name.c_str(),
                                    _tag_table.data.size(),
                                    _tag_table.dst_size,
                                    _tag_table.dst_offset,
                                    _tag_table.dst_sizes,
                                    _tag_table.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records TAGS returned:"+std::to_string(err));

    _tag_table.data.clear();

    err = H5TBappend_records(_log_table.gid,
                                    _log_table.table_name.c_str(),
                                    _log_table.data.size(),
                                    _log_table.dst_size,
                                    _log_table.dst_offset,
                                    _log_table.dst_sizes,
                                    _log_table.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records LOG returned:"+std::to_string(err));

    _log_table.data.clear();

    err = H5TBappend_records(_command_table.gid,
                             _command_table.table_name.c_str(),
                             _command_table.data.size(),
                             _command_table.dst_size,
                             _command_table.dst_offset,
                             _command_table.dst_sizes,
                             _command_table.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records COMMAND returned:"+std::to_string(err));

    _command_table.data.clear();

    for(auto& m: _pols){
        for(Polarimeter& pol : m.second){
            herr_t err = H5TBappend_records(pol.gid,
                                            pol.table_name.c_str(),
                                            pol.data.size(),
                                            pol.dst_size,
                                            pol.dst_offset,
                                            pol.dst_sizes,
                                            pol.data.data());
            if(err != 0)
                utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records POLARIMETER returned:"+std::to_string(err));
            pol.data.clear();

            for(auto& board : _hk)
                for(auto& pol : board.second)
                    for(auto& table : pol.second)
                        for(auto& hh : table.second){
                            if (hh.second.data.empty()){
                                continue;
                            }
                            HK& h = hh.second;
                            herr_t err = H5TBappend_records(h.gid,
                                                            h.table_name.c_str(),
                                                            h.data.size(),
                                                            h.dst_size,
                                                            h.dst_offset,
                                                            h.dst_sizes,
                                                            h.data.data());
                            if(err != 0)
                                utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records HK returned:"+std::to_string(err));
                            h.data.clear();

                        }
        }
    }

    for(auto& c : _cryo){
        herr_t err = H5TBappend_records(c.second.gid,
                                        c.second.table_name.c_str(),
                                        c.second.data.size(),
                                        c.second.dst_size,
                                        c.second.dst_offset,
                                        c.second.dst_sizes,
                                        c.second.data.data());
        if(err != 0)
            utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records HK returned:"+std::to_string(err));
        c.second.data.clear();   
    }

    _file->flush(H5F_SCOPE_LOCAL); // let SWMR work properly


/*    high_resolution_clock::time_point t2 = high_resolution_clock::now(); //DBG
    duration<double> t0_t1 = duration_cast<duration<double>>(t1 - t0); //DBG
    duration<double> t1_t2 = duration_cast<duration<double>>(t2 - t1); //DBG
    std::cout << "write columns took: " << t0_t1.count()
              << " flush took: " << t1_t2.count() << std::endl;
*/
}

class HKvisitor : public boost::static_visitor<void>{
public:
    HKvisitor(H5Stream::HK_data& d) :_d(d){}

    void operator()(const int64_t& i) const
    {
        _d.i_val = i;
    }

    void operator()(const uint64_t& i) const
    {
        _d.u_val = i;
    }

    template<class T>
    void operator()(const T& i) const
    {
        std::cerr << "HK type error" << std::endl;
    }

    H5Stream::HK_data& _d;
};

void  H5Stream::imp_hk(std::shared_ptr<const msgpack::type::variant>& pkt){
    typedef std::map<msgpack::type::variant,msgpack::type::variant> mpmap;
    try {
        std::string board_name;
        std::string pol_name;
        std::string table;
        std::string address;


        const mpmap& m = pkt->as_map();
        double mjd = boost::get<double>(m.at("mjd"));
        if(m.find("board") != m.end()){
            board_name = boost::get<std::string>(m.at("board"));
            pol_name   = "SYS";
        }else if(m.find("pol") != m.end()){
            pol_name = boost::get<std::string>(m.at("pol"));
            bool foud = false;
            for(const auto& daq : _pols){
                for(const auto& pol : daq.second){
                    if(pol.pol_name == pol_name){
                        board_name = pol.board_name;
                        foud = true;
                        break;
                    }
                }
                if(foud) break;
            }
        }
        if(m.find("bias") != m.end())
            table = "bias";
        else if(m.find("daq") != m.end())
            table = "daq";

        if(board_name.empty() || pol_name.empty() || table.empty()){
            std::cerr << board_name << " -> "
                      << pol_name   << " -> "
                      << table      << ": not valid " << std::endl;
            return;
        }

        for(const auto& addr : m.at(table).as_map()){
            HK_data d;
            d.m_jd = mjd;
            HK& h = _hk.at(board_name).at(pol_name).at(table).at(boost::get<std::string>(addr.first));
            HKvisitor visitor(d);


            boost::apply_visitor(visitor,addr.second);
/*            if(h.type == addr_item::Type::u16)

                d.u_val = boost::get<uint64_t>(addr.second);
            else
                d.i_val = boost::get<int64_t>(addr.second);
*/
            h.data.push_back(d);
        }

    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
    }

}

void H5Stream::imp_add(std::shared_ptr<const DaqUdpPkt>& pkt){
    char id = pkt->pkts[0].board_id;
    if(_time_corr.reference_board == _time_corr.NULL_BOARD)
        _time_corr.reference_board = id;

    for(const DaqPkt& p : pkt->pkts){
        double ts = utils::Time::get_MJD(p.timestamp);
        if (_time_corr.reference_board == id){
            _time_corr.data.push_back({ts,p.timestamp.tv_sec,p.timestamp.tv_usec,p.phb});
        }
        for(size_t i=0; i<_pols[id].size(); i++){
            _pols[id][i].data.push_back(Pol_data());
            Pol_data& d = _pols[id][i].data.back();
            d.m_jd = ts;
            d.flags = _pols[id][i].current_flag;
            d.dem_q1 = p.dem_Q1(i);
            d.dem_u1 = p.dem_U1(i);
            d.dem_u2 = p.dem_U2(i);
            d.dem_q2 = p.dem_Q2(i);

            d.pwr_q1 = p.pwr_Q1(i);
            d.pwr_u1 = p.pwr_U1(i);
            d.pwr_u2 = p.pwr_U2(i);
            d.pwr_q2 = p.pwr_Q2(i);
        }
    }
}

bool H5Stream::imp_open(const std::string& file_name){
    bool is_ok = true;
    _groups.clear();
    _file.reset(new H5::H5File(file_name, H5F_ACC_TRUNC | H5F_ACC_SWMR_WRITE, H5P_DEFAULT, H5P_DEFAULT ));
    _groups.push_back(_file->createGroup("/POSITION"));
    _tel.gid = _groups.back().getId();
    _tel.current_flag = 0;

    _groups.push_back(_file->createGroup("/TAGS"));
    _tag_table.gid =  _groups.back().getId();

    _groups.push_back(_file->createGroup("/TIME_CORRELATION"));
    _time_corr.gid = _groups.back().getId();

    _groups.push_back(_file->createGroup("/LOG"));
    _log_table.gid = _groups.back().getId();

    _groups.push_back(_file->createGroup("/COMMANDS"));
    _command_table.gid = _groups.back().getId();

    // clear HK ref
/*    for(auto& board : _hk)
        for(auto& pol : board.second)
            for(auto& table : pol.second)
                for(auto& h : table.second){
                    h.second.ref.m_jd = -1.0;
                }
*/
    for(auto& daq : _pols){

        for(uint8_t i=0; i<daq.second.size(); i++){
            std::string g_name = "/"+daq.second[i].group_name;
            _groups.push_back(_file->createGroup(g_name));

            daq.second[i].gid = _groups.back().getId();
            daq.second[i].current_flag = 0;

            HKkey key;
            key.board_id = daq.first;
            key.pol = i;

            std::string board_name; // ok
            std::string pol_name;
            std::string table_name;
            std::string addr_name;


            g_name = "/"+daq.second[i].group_name+"/BIAS";
            _groups.push_back(_file->createGroup(g_name));
            hid_t cgid = _groups.back().getId();
            key.table   = "bias";
            for(const addr_item& it : singletons.config->address_bias_pol){
                _hk[daq.second[i].board_name][daq.second[i].pol_name]["bias"][it.name].gid = cgid;
            }

            g_name = "/"+daq.second[i].group_name+"/DAQ";
            _groups.push_back(_file->createGroup(g_name));
            cgid = _groups.back().getId();
            key.table   = "daq";
            for(const addr_item& it : singletons.config->address_daq_pol){
                key.addr     = it.address;
                _hk[daq.second[i].board_name][daq.second[i].pol_name]["daq"][it.name].gid = cgid;
            }

        }
        auto pop_group = [&](const std::string& hk_src,std::list<addr_item>& table,hid_t gid){
            for(const addr_item& it : table){
                _hk[daq.second[0].board_name]["SYS"][hk_src][it.name].gid = gid;
            }
        };

        std::string g_name = "/BOARD_"+singletons.config->daq_id.at(daq.first)->name;
        _groups.push_back(_file->createGroup(g_name));

        g_name = "/BOARD_"+singletons.config->daq_id.at(daq.first)->name+"/BIAS";
        _groups.push_back(_file->createGroup(g_name));
        pop_group("bias",singletons.config->address_bias_sys,_groups.back().getId());

        g_name = "/BOARD_"+singletons.config->daq_id.at(daq.first)->name+"/DAQ";
        _groups.push_back(_file->createGroup(g_name));
        pop_group("daq", singletons.config->address_daq_sys, _groups.back().getId());

    }

    _groups.push_back(_file->createGroup("/CRYO"));
    for(const auto& probe : singletons.config->cryo_config){
        std::string g_name = "/CRYO/" + probe.id;
        _groups.push_back(_file->createGroup(g_name));
        _cryo[probe.id].gid = _groups.back().getId();
    }

    auto make_table = [](auto& obj) -> bool {
        herr_t err = H5TBmake_table( obj.table_title,
                                    obj.gid,
                                    obj.table_name.c_str(),
                                    obj.num_fields,
                                    0,
                                    obj.dst_size,
                                    obj.field_names,
                                    obj.dst_offset,
                                    obj.field_type,
                                    obj.chunk,
                                    nullptr, 0, nullptr);
        return err == 0;
    };

    is_ok &= make_table(_time_corr);
    is_ok &= make_table(_tel);
    is_ok &= make_table(_log_table);
    is_ok &= make_table(_command_table);
    is_ok &= make_table(_tag_table);

    for(auto& daq : _pols){
        for(Polarimeter& pol : daq.second){
            is_ok &= make_table(pol);
        }
    }

    for(auto& board : _hk)
        for(auto& pol : board.second)
            for(auto& table : pol.second)
                for(auto& h : table.second){
                    is_ok &= make_table(h.second);
                }

    for(auto& c : _cryo)
        is_ok &= make_table(c.second);

    std::shared_ptr<H5FileInfo> info(new H5FileInfo());
    info->db_id = 0;
    info->relative_path = relative_path();

    signal_hdf5_file::deque_future fut = _sig_file(info);

    auto s = fut.size();
    if(s == 0){
        utils::Log::log(utils::LogMessage::Level::WARNING,class_name,"no DB registred for storing hdf5 file info");
    }else if(s != 1){
        utils::Log::log(utils::LogMessage::Level::WARNING,class_name,"multiple DBs registred for storing hdf5 file info, using first");
    }

    if(s > 0){
        _db_info = std::move(fut[0]);
    }
    std::vector<std::shared_ptr<const DbTag>> active_tags = singletons.tag_control->get_active();

    for(auto& ptr : active_tags){
      imp_tag(ptr); //add tags letf open from previos sessions
    }


    return is_ok;
}


void H5Stream::write_final_attributes(H5FileInfo& info){
    using namespace H5;
    FloatType double_type(PredType::NATIVE_DOUBLE);
    DataSpace att_space(H5S_SCALAR);
    Attribute fs = _file->createAttribute("FIRST_SAMPLE", double_type, att_space);
    Attribute ls = _file->createAttribute("LAST_SAMPLE", double_type, att_space);

    double min_mjd = -1;
    double max_mjd = -1;

    auto update_stats = [](
            H5Stream::Polarimeter& pol,
            double& min_mjd,
            double& max_mjd,
            std::unique_ptr<double>& start_mjd,
            std::unique_ptr<double>& stop_mjd){
        H5Stream::Pol_data data;
        herr_t err = H5TBread_records(
                    pol.gid,
                    pol.table_name.c_str(),
                    0,
                    1,
                    pol.dst_size,
                    pol.dst_offset,
                    pol.dst_sizes,
                    &data);
        if(err < 0)
            return;

        if(min_mjd <= 0){
            min_mjd = data.m_jd;
        }else{
            min_mjd = std::min(data.m_jd,min_mjd);
        }
        start_mjd.reset(new double(data.m_jd));


        hsize_t nfields, nrecords;

        err = H5TBget_table_info ( pol.gid, pol.table_name.c_str(), &nfields, &nrecords );
        if(err < 0)
            return;

        err = H5TBread_records(
                    pol.gid,
                    pol.table_name.c_str(),
                    nrecords-1,
                    1,
                    pol.dst_size,
                    pol.dst_offset,
                    pol.dst_sizes,
                    &data);
        if(err < 0)
            return;

        if(max_mjd <= 0){
            max_mjd = data.m_jd;
        }else{
            max_mjd = std::max(data.m_jd,max_mjd);
        }
        stop_mjd.reset(new double(data.m_jd));
    };

    typedef struct{
            std::string name;
            double& mjd_start;
            double& mjd_stop;
            std::unique_ptr<double>& b_mjd_start;
            std::unique_ptr<double>& b_mjd_stop;
    }tuple;


    tuple stats[NUM_BOARDS]{
        {"POL_R0",min_mjd,max_mjd,info.R_mjd_start,info.R_mjd_stop},
        {"POL_O0",min_mjd,max_mjd,info.O_mjd_start,info.O_mjd_stop},
        {"POL_Y0",min_mjd,max_mjd,info.Y_mjd_start,info.Y_mjd_stop},
        {"POL_G0",min_mjd,max_mjd,info.G_mjd_start,info.G_mjd_stop},
        {"POL_B0",min_mjd,max_mjd,info.B_mjd_start,info.B_mjd_stop},
        {"POL_I0",min_mjd,max_mjd,info.I_mjd_start,info.I_mjd_stop},
        {"POL_V0",min_mjd,max_mjd,info.V_mjd_start,info.V_mjd_stop}
    };

    for(tuple& t :stats){
        bool found = false;
        for(auto& daq: _pols){
            if(daq.second.size() > 0 && daq.second[0].group_name == t.name){
                found = true;
                update_stats(daq.second[0],t.mjd_start,t.mjd_stop,t.b_mjd_start,t.b_mjd_stop);
                break;
            }
        }
        if(! found){
            utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"cannot find polarimeter "+ t.name +" while updating stats");
        }
    }

    fs.write(double_type,&min_mjd);
    ls.write(double_type,&max_mjd);

    fs.setComment("first time stamp (mjd) of all polarimeters");
    ls.setComment("last time stamp (mjd) of all polarimeters");

    info.mjd_start.reset(new double(min_mjd));
    info.mjd_stop.reset(new double(max_mjd));

    H5Stream::Time_corr_data data;
    herr_t err = H5TBread_records(
                _time_corr.gid,
                _time_corr.table_name.c_str(),
                0,
                1,
                _time_corr.dst_size,
                _time_corr.dst_offset,
                _time_corr.dst_sizes,
                &data);
    if(err < 0)
        return;

    info.board_unix_s_start.reset(new int64_t(data.unix_time_s));
    info.board_unix_us_start.reset(new int64_t(data.unix_time_us));

    hsize_t nfields, nrecords;

    err = H5TBget_table_info ( _time_corr.gid, _time_corr.table_name.c_str(), &nfields, &nrecords );
    if(err < 0)
        return;

    err = H5TBread_records(
                _time_corr.gid,
                _time_corr.table_name.c_str(),
                nrecords-1,
                1,
                _time_corr.dst_size,
                _time_corr.dst_offset,
                _time_corr.dst_sizes,
                &data);
    if(err < 0)
        return;

   info.board_unix_s_stop.reset(new int64_t(data.unix_time_s));
   info.board_unix_us_stop.reset(new int64_t(data.unix_time_us));
}

void H5Stream::finalize_tags(){
    for(auto it=_tags.begin(); it != _tags.end(); it++){
        Tag_data t = it->second;
        it->second.m_jd_end = -1;
        _tag_table.data.push_back(t);
    }

    if(! _tag_table.data.empty()){

    herr_t err = H5TBappend_records(_tag_table.gid,
                                    _tag_table.table_name.c_str(),
                                    _tag_table.data.size(),
                                    _tag_table.dst_size,
                                    _tag_table.dst_offset,
                                    _tag_table.dst_sizes,
                                    _tag_table.data.data());
    if(err != 0)
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"H5TBappend_records TAGS returned:"+std::to_string(err));
}
    _tag_table.data.clear();
}


void H5Stream::imp_close(){
    /**********************************
    size_t maxq = 0;
    for(signal_pkt::Connection& c : _conn_pkt){
        maxq = std::max(maxq,c->max_queue);
        c->max_queue=0;
    }


    utils::Log::log(utils::LogMessage::Level::INFO,
                    class_name,
                    "MAXQ HDF5: "+std::to_string(maxq));
    //*************************************************/
    std::shared_ptr<H5FileInfo> info;
    try{
        info.reset(new H5FileInfo(_db_info.get())); //get instance saved n DB
    }catch (std::future_error& e) { // DB could not save the instance
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"could not retrive db saved info: "+ e.code().message());
        info.reset(new H5FileInfo());
        info->db_id = 0;
    }

    write_final_attributes(*info);
    finalize_tags();
    for(H5::Group& g : _groups)
        g.close();
    _file->close();
    H5garbage_collect(); //DBG try to free memory

    _file.reset();

    _tel.data.clear();
    _tel.gid = -1;

    _tag_table.data.clear();
    _tag_table.gid = -1;

    for(auto& daq : _pols){
        for(Polarimeter& p : daq.second){
            p.data.clear();
            p.gid = -1;
        }
    }

    for(auto& board : _hk)
        for(auto& pol : board.second)
            for(auto& table : pol.second)
                for(auto& h : table.second){
                    h.second.data.clear();
                    h.second.gid = -1;
                }

    for(auto& c: _cryo){
        c.second.data.clear();
        c.second.gid = -1;
    }

    info->relative_path = relative_path();

    _sig_file(info);
}

void H5Stream::imp_pol_flag(std::shared_ptr<const PolarimeterFlag>& f){
    uint8_t flag = f->get();
    bool clear = f->clear;

    std::vector<size_t> idx;

    if(singletons.config->daq_name.find(f->board_name) == singletons.config->daq_name.end()){
         utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"invalid board name for flag: "+ f->board_name);
         return;
    }
    char board = singletons.config->daq_name.at(f->board_name)->id;

    auto do_flag = [flag,clear](Polarimeter& p){
        if(clear)
            p.current_flag &= flag;
        else
            p.current_flag |= flag;
    };

    if(_pols.find(board) == _pols.end()){
        utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"invalid board id for flag: "+board);
        return;
    }
    if(f->pol_no == SloControl::BOARD_ADDR){ // ALL POLARIMETERS IN THE BOARD
        for(auto& pol : _pols.at(board))
            do_flag(pol);
    }else{
        if(f->pol_no > _pols.at(board).size()-1){
            utils::Log::log(utils::LogMessage::Level::ERROR,class_name,"invalid polarimeter numbere for flag: "+ std::to_string(int(f->pol_no)));
            return;
        }else{
            do_flag(_pols.at(board)[f->pol_no]);
        }
    }

}

void H5Stream::imp_tel_flag(std::shared_ptr<const TelescopeFlag>& f){
    if(f->clear)
        _tel.current_flag &= f->get();
    else
        _tel.current_flag |= f->get();
}


void H5Stream::imp_tag(std::shared_ptr<const DbTag> &tag){
                    using namespace H5;
        auto it = _tags.find(tag->tag);

        Tag_data t;
        t.id = tag->db_id;
        t.m_jd_start = tag->mjd_start;
        t.m_jd_end   = tag->mjd_stop;
        std::memset(t.tag,0,sizeof(t.tag));
        std::memset(t.start_comment,0,sizeof(t.start_comment));
        std::memset(t.end_comment,0,sizeof(t.end_comment));

        tag->tag.copy(t.tag,sizeof(t.tag));

        if(! tag->comment_start.empty())
            tag->comment_start.copy(t.start_comment,sizeof(t.start_comment));

        if(! tag->comment_stop.empty())
            tag->comment_stop.copy(t.end_comment,sizeof(t.end_comment));

        if(t.m_jd_end < 0){//START
            _tags[tag->tag] = t;
        }else{//END
            if(it != _tags.end())
                _tags.erase(it);
            _tag_table.data.push_back(t);
        }
}

void H5Stream::slot_log(std::shared_ptr<const utils::LogMessage> log){
   Log_data ld;
   memset(ld.emit_class,'\0',sizeof(ld.emit_class));
   memset(ld.user,'\0',sizeof(ld.user));
   memset(ld.message,'\0',sizeof(ld.message));

   ld.level = log->level;
   ld.m_jd  = utils::Time::get_unix_MJD(std::chrono::duration_cast<std::chrono::nanoseconds>(log->timestamp.time_since_epoch()).count());
   log->emit_class.copy(ld.emit_class,sizeof(ld.emit_class),0);
   log->user.copy(ld.user,sizeof(ld.user),0);
   log->message.copy(ld.message,sizeof(ld.message),0);

   std::unique_lock<std::recursive_mutex> l(_m);
   _log_table.data.push_back(ld);
}

void H5Stream::connect_log(utils::signal_log &sig){
    _conn_log = sig.connect(std::bind(&H5Stream::slot_log, std::ref(*this),std::placeholders::_1));
}

void H5Stream::connect_req(signal_slo_req &sig){
    signal_slo_req::Connection conn = sig.connect(std::bind(&H5Stream::slot_req, std::ref(*this),std::placeholders::_1));
    _conn_req.push_back(conn);
}

void H5Stream::connect_ack(signal_slo_req &sig){
    signal_slo_req::Connection conn = sig.connect(std::bind(&H5Stream::slot_ack, std::ref(*this),std::placeholders::_1));
    _conn_ack.push_back(conn);
}

void H5Stream::connect_cryo(signal_msgpack& sig){
    _conn_cryo = sig.connect(std::bind(&H5Stream::slot_cryo, std::ref(*this),std::placeholders::_1));
}

void H5Stream::do_slot_command(std::shared_ptr<const SloCommand>& req, bool is_ack){
    Command_data d;

    memset(d.board_name,'\0',sizeof(d.board_name));
    memset(d.base_addr,'\0',sizeof(d.base_addr));
    memset(d.message,'\0',sizeof(d.message));
    memset(d.data,'\0',sizeof(d.data));
    memset(d.user,'\0',sizeof(d.user));

    req->board_name.copy(d.board_name,sizeof(d.board_name),0);
    d.pol_no = req->pol_no;
    d.method = req->method;
    d.type = req->type;
    req->base_addr.copy(d.base_addr,sizeof(d.base_addr),0);
    d.counter = req->counter;
    req->user.copy(d.user,sizeof(d.user),0);
    req->message.copy(d.message,sizeof(d.message),0);
    d.m_jd = utils::Time::get_unix_MJD(std::chrono::duration_cast<std::chrono::nanoseconds>(req->timestamp.time_since_epoch()).count());
    d.req_ack = is_ack ? Command_data::Ack::ACK : Command_data::Ack::REQ;
    d.res = req->res;

    std::stringstream ss;
    ss << "[" << std::hex ;
    for(auto i=0; i<req->data.size()-1; i++)
        ss << req->data[i] << ",";
    ss << req->data.back() << "]";

    std::string data = ss.str();
    data.copy(d.data,sizeof(d.data),0);

    std::unique_lock<std::recursive_mutex> l(_m);
    _command_table.data.push_back(d);
}


void H5Stream::slot_cryo(std::shared_ptr<const msgpack::type::variant> pkt){
    try
    {
        std::string id;
        Cryo_data data;
        const auto& map = pkt->as_map();
        id = map.at("id").as_string();
        data.m_jd = map.at("mjd").as_double();
        data.raw = map.at("ret").as_vector().front().as_double();
        data.cal = map.at("calibrated").as_double();
        _cryo.at(id).data.push_back(data);
    }
    catch (const std::exception &e)
    {
        std::cout << "HDF5StreamError[Cryo]: " << e.what() << std::endl;
    }
}

}
