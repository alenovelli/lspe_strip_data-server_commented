#include "../../include/strip/daq_pkt.hpp"

#include <boost/endian/conversion.hpp>
#include <utility> // std::pair, std::make_pair
#include <iostream>
#include <sys/time.h>

#define CENT2MICRO 10000

namespace strip
{

bool DaqPkt::decode(const iterator &begin, const iterator &end, timeval *ts_offset)
{
    //  std::cout << "decode" << std::endl;
    iterator b = begin;
    iterator e = end;
    if (!is_valid(b, e))
        return false;

    board_id = b[BOARD_ID];

    phb = b[PH_ID] - 0x30;
    decode_times(b);

    /* calculate offset and apply*/
    if (ts_offset)
    {
        if (!timerisset(ts_offset))
        {
            timeval t;
            gettimeofday(&t, nullptr);
            timersub(&t, &timestamp, ts_offset);
        }
        timeval t = timestamp;
        timeradd(&t, ts_offset, &timestamp);

    }
    decode_channels(b);
    return true;
}

bool DaqPkt::encode(const iterator &begin, const iterator &end) const
{
    iterator b = begin;
    iterator e = end;
    if (e - b != DAQ_PKT_LEN)
        return false; //wrong lenght
    b[HEADER_D] = 'D';
    b[HEADER_A] = 'A';
    b[HEADER_Q] = 'Q';
    b[BOARD_ID] = board_id;
    b[PH_ID] = phb + 0x30;

    encode_times(b);
    encode_channels(b);
    return true;
}

void DaqPkt::decode_times(iterator s)
{

    /*    timeval t;
    timeval t_off;

    t_off.tv_sec = 0;
    t_off.tv_usec = (s[CENTS] % 100) * 10000;

    gettimeofday(&t,nullptr);
    timeradd(&t,&t_off,&timestamp);

    phb           = s[PHASE_B];
    cents         = s[CENTS];
    n_samples     = s[N_SAMPLES];
    blanking_time = s[BLANK_TIME];
*/
    brst_pls = s[BRST_PLS];
    brst_lat = s[BRST_LAT];

    sec_h = s[SEC_H];

    uint32_t posix_time = boost::endian::big_to_native(*reinterpret_cast<uint32_t *>(s + POSIX_TIME));
    timestamp.tv_usec = sec_h * CENT2MICRO;
    timestamp.tv_sec = posix_time;
}

void DaqPkt::encode_times(iterator s) const
{
    s[BRST_PLS] = brst_pls;
    s[BRST_LAT] = brst_lat;

    uint32_t *posix_time = reinterpret_cast<uint32_t *>(s + POSIX_TIME);
    *posix_time = timestamp.tv_sec;
    boost::endian::native_to_big_inplace(*posix_time);

    uint8_t cents = timestamp.tv_usec / CENT2MICRO;
    s[SEC_H] = cents;
}

void DaqPkt::decode_channels(iterator s)
{
    s += SCI_DATA_0;
    for (Channel &c : channels)
    {
        c.dem = boost::endian::little_to_native(*reinterpret_cast<int32_t *>(s));
        s += 4;
        c.tpw = boost::endian::little_to_native(*reinterpret_cast<uint32_t *>(s));
        s += 4;
    }
}

void DaqPkt::encode_channels(iterator s) const
{
    s += SCI_DATA_0;
    for (const Channel &c : channels)
    {
        int32_t *dem = reinterpret_cast<int32_t *>(s);
        *dem = c.dem;
        boost::endian::native_to_little_inplace(*dem);
        s += 4;

        uint32_t *tpw = reinterpret_cast<uint32_t *>(s);
        *tpw = c.tpw;
        boost::endian::native_to_little_inplace(*tpw);
        s += 4;
    }
}

bool DaqPkt::is_valid(iterator b, iterator e)
{
    if (e - b != DAQ_PKT_LEN)
    {
        std::cerr << "wrong packet lenght" << std::endl;
        return false;
    } //wrong lenght
    if (b[HEADER_D] != 'D')
    {
        std::cerr << "'D' expected" << std::endl;
        return false;
    }
    if (b[HEADER_A] != 'A')
    {
        std::cerr << "'A' expected" << std::endl;
        return false;
    }
    if (b[HEADER_Q] != 'Q')
    {
        std::cerr << "'Q' expected" << std::endl;
        return false;
    }
    if (b[BOARD_ID] < '0' || b[0x003] > '8')
    {
        std::cerr << "board ID out of range" << std::endl;
        return false;
    }

    return true;
}

} // namespace strip
