#include "../../include/strip/auto_dispatcher.hpp"
#include "../../include/strip/singleton.hpp"

namespace strip {

const std::string AutoDispatcher::class_name = "AutoDispatcher";
AutoDispatcher::AutoDispatcher() :
    _pkt(singletons.workers_pool),
    _pol_flag(singletons.workers_pool),
    _tel_flag(singletons.workers_pool)
{
    _conn_close = utils::UnixSignals::halt2_ops().connect(std::bind(&AutoDispatcher::slot_close, this));
}

void AutoDispatcher::connect_pkt(signal_pkt& sig){
    signal_pkt::Connection conn = sig.connect(std::bind(&AutoDispatcher::slot_pkt, std::ref(*this),std::placeholders::_1));
    _conn_pkt.push_back(conn);
}

void AutoDispatcher::connect_pol_flag(signal_pol_flag &sig){
    signal_pol_flag::Connection conn = sig.connect(std::bind(&AutoDispatcher::slot_pol_flag, std::ref(*this),std::placeholders::_1));
    _conn_pol_flag.push_back(conn);
}

void AutoDispatcher::connect_tel_flag(signal_tel_flag &sig){
    signal_tel_flag::Connection conn = sig.connect(std::bind(&AutoDispatcher::slot_tel_flag, std::ref(*this),std::placeholders::_1));
    _conn_tel_flag.push_back(conn);
}


void AutoDispatcher::slot_pkt(std::shared_ptr<const DaqUdpPkt> pkt){
    _pkt(pkt);
}

void AutoDispatcher::slot_pol_flag(std::shared_ptr<const PolarimeterFlag> flag){
    _pol_flag(flag);
}

void AutoDispatcher::slot_tel_flag(std::shared_ptr<const TelescopeFlag> flag){
    _tel_flag(flag);
}

int AutoDispatcher::slot_close(){
    return 0;
}

void AutoDispatcher::addAFpkt(std::shared_ptr<AFpkt> f){
    f->connect_pkt(_pkt);
    _af_pkt.push_back(f);
}

void AutoDispatcher::addAFpol_flag(std::shared_ptr<AFpol_flag> f){
    f->connect_pol_flag(_pol_flag);
    _af_pol_flag.push_back(f);
}

void AutoDispatcher::addAFtel_flag(std::shared_ptr<AFtel_flag> f){
    f->connect_tel_flag(_tel_flag);
    _af_teg_flag.push_back(f);
}

void AFpkt::connect_pkt(signal_pkt& sig){
    signal_pkt::Connection conn = sig.connect(std::bind(&AFpkt::slot_pkt, std::ref(*this),std::placeholders::_1));
    _conn_pkt.push_back(conn);
}

void AFpkt::slot_pkt(std::shared_ptr<const DaqUdpPkt> pkt){
    do_pkt(pkt);
}

void AFpol_flag::connect_pol_flag(signal_pol_flag &sig){
    signal_pol_flag::Connection conn = sig.connect(std::bind(&AFpol_flag::slot_pol_flag, std::ref(*this),std::placeholders::_1));
    _conn_pol_flag.push_back(conn);
}

void AFpol_flag::slot_pol_flag(std::shared_ptr<const PolarimeterFlag> flag){
    do_pol_flag(flag);
}

void AFtel_flag::connect_tel_flag(signal_tel_flag &sig){
    signal_tel_flag::Connection conn = sig.connect(std::bind(&AFtel_flag::slot_tel_flag, std::ref(*this),std::placeholders::_1));
    _conn_tel_flag.push_back(conn);
}

void AFtel_flag::slot_tel_flag(std::shared_ptr<const TelescopeFlag> flag){
    do_tel_flag(flag);
}

}
