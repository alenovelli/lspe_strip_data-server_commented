#include "daq_pkt.hpp"
#include "strip_utils.hpp"
#include "slo_request_struct.hpp"
#include "slo_control.hpp"
#include "strip_singleton.hpp"

#include <mutex>
#include <vector>
#include <memory>
#include <map>
#include <list>
#include <iostream>
#include <sstream>
#include <thread>
#include <random>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <limits>
#include <atomic>
#include <thread>
#include <random>

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#define MEM_CELLS    128
#define MEM_RO      0x50
constexpr  uint8_t HK_SRC_BIAS = strip::DaqPkt::HK_SRC_BIAS;
constexpr  uint8_t HK_SRC_DAQ  = strip::DaqPkt::HK_SRC_DAQ;

using namespace strip;

class BoardMem{
public:
    typedef std::map<uint8_t,std::vector<strip::HKvalue>> table_map;
    typedef std::map<uint8_t,table_map> hk_src_map;

    hk_src_map mem;
    BoardMem(){
        for (uint8_t pol : {0,1,2,3,4,5,6,7,0xF}){
            mem[HK_SRC_DAQ ][pol].assign(MEM_CELLS,{0});
            mem[HK_SRC_BIAS][pol].assign(MEM_CELLS,{0});
        }
    }
};

BoardMem board_memory;

class SloDAQ {
    static constexpr uint64_t BUFFER_SIZE = 2048;
public:
    SloDAQ(boost::asio::ip::address &addr, uint16_t port,BoardMem* mem);


private:
    void start_receive();
    void handle_receive(const boost::system::error_code& error,
                        std::size_t bytes_transferred);
    boost::asio::ip::udp::socket _socket;
    BoardMem* _mem;
    boost::asio::ip::udp::endpoint _remote_endpoint;
    char _buffer[BUFFER_SIZE];

    void send_back(const std::string& err_msg, bool is_ok);
};

uint64_t delay = 0;
using boost::asio::ip::udp;

SloDAQ::SloDAQ(boost::asio::ip::address& addr, uint16_t port, BoardMem *mem) :
    _socket(strip::utils::ioService::service, udp::endpoint(addr, port)), _mem(mem){

    start_receive();

}

void SloDAQ::start_receive(){
    memset(_buffer,'\0',BUFFER_SIZE);
    _socket.async_receive_from(
                boost::asio::buffer(_buffer), _remote_endpoint,
                boost::bind(&SloDAQ::handle_receive, this,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
}


void SloDAQ::send_back(const std::string &err_msg, bool is_ok){
    std::this_thread::sleep_for (std::chrono::milliseconds(delay));
    std::string counter = "";
    if(is_ok){
        auto it = std::find(_buffer,&_buffer[BUFFER_SIZE],'|');
        if(it != &_buffer[BUFFER_SIZE])
            counter = std::string(it);
    }
    std::string str_msg = err_msg + counter + "\r";
    std::cout << str_msg << std::endl;
    std::cout << "---------------------------------------" << std::endl;
    _socket.send_to(boost::asio::buffer(str_msg), _remote_endpoint);
    start_receive();
}

void SloDAQ::handle_receive(const boost::system::error_code &error, std::size_t bytes_transferred){
    std::string message = "";
    if(bytes_transferred == 0) //might happen on close
        return start_receive();

    if(error){
        std::cout << "ERROR" << error.message() << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }

    std::cout << _buffer << std::endl;

    //DECODING
    if(_buffer[0] != '#'){
        std::cout << "SYNTAX ERROR: command should begin with '#'" << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }


    if(_buffer[1] != 'B' && _buffer[1] != 'P'){
        std::cout << "SYNTAX ERROR: command not 'B' nor 'P'" << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }

    int pol_no = -1;

    if(_buffer[2] >= '0' && _buffer[2] <='7'){
        pol_no = _buffer[2] - '0';
    }else if(_buffer[2] == 'F' || _buffer[2] == 'f'){
        pol_no = 15;
    }else{
        std::cout << "SYNTAX ERROR: invalid polarimeter number" << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }

    std::string::size_type sz;
    const int addr = std::stoi(&_buffer[3],&sz,16);

    sz += 3;

    if(_buffer[sz] != '?' && _buffer[sz] != '='){
        std::cout << "SYNTAX ERROR: '?' nor '=' found" << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }

    if(_buffer[bytes_transferred-1] != '\r'){
        std::cout << "SYNTAX ERROR: final  '\\r' missing " << std::endl;
        return send_back("#B_UNKNOWN_COMMAND",false);
    }
    if(_buffer[sz] == '?'){
        sz++;
        int num = 0;
        if(_buffer[sz] == '\r' || _buffer[sz] == '|')
            num = 1;
        else{
            size_t ssz = sz;
            num = std::stoi(&_buffer[ssz],&sz,16);
        }
        //command ok, execute
        std::cout << "COMMAND OK" << std::endl;
        //TODO execute
        std::vector<uint16_t> values;

        auto get_fun = [&](uint8_t hk_src, uint8_t pol) -> bool{
            uint8_t _addr = addr;
            for(auto i=0; i<num;i++){
                if(_addr >= MEM_CELLS){
                    std::cout << "BAD_ADDRESS: "<< _addr << std::endl;
                    message = "BAD_ADDRESS";
                    return false;
                }
                _addr++;
                values.push_back(_mem->mem[hk_src][pol][_addr].u_val);
            }
            return true;
        };

        bool is_ok;
        if(_buffer[1] == 'B'){
            is_ok = get_fun(strip::DaqPkt::HK_SRC_BIAS,pol_no);
        }else
            is_ok = get_fun(strip::DaqPkt::HK_SRC_DAQ,pol_no);

        if(! is_ok){
            std::stringstream ss;
            ss << "#" << _buffer[1] << "_" << message;
            send_back(ss.str(),true);
        }else{
            std::stringstream ss;
            ss << "#" << _buffer[1] << "_TLM:"
               << std::hex << std::uppercase << pol_no << ";" << addr << "=";

            for(auto i=0; i<values.size()-1;i++)
                ss << std::hex << std::uppercase << values[i] << ",";
            ss << std::hex << std::uppercase << values.back();
            send_back(ss.str(),true);
        }

    }else{ // '='
        std::vector<int> values;
        sz++;
        size_t ssz = sz;
        values.push_back(std::stoi(&_buffer[ssz],&sz,16));
        ssz += sz;
        while(_buffer[ssz] == ','){
            ssz++;
            std::cout<<"REMAIN:" << &_buffer[ssz] << std::endl;
            values.push_back(std::stoi(&_buffer[ssz],&sz,16));
            ssz += sz;
        }

        //command ok, execute
        std::cout << "COMMAND OK" << std::endl;

        //TODO execute
        auto set_fun = [&](uint8_t hk_src, uint8_t pol) -> bool{
            int _addr = addr;
            for(uint16_t v : values){
                if(_addr >= MEM_CELLS){
                    std::cout << "BAD_ADDRESS: "<< _addr << std::endl;
                    message = "BAD";
                    return false;
                }
                if(_addr >= MEM_RO){
                    std::cout << "READ ONLY ADDRESS: "<< _addr << std::endl;
                    message = "ROR";
                    return false;
                }
                _addr++;
                _mem->mem[hk_src][pol][_addr].u_val = v;
            }
            return true;
        };

        bool is_ok;
        if(_buffer[1] == 'B'){
            is_ok = set_fun(strip::DaqPkt::HK_SRC_BIAS,pol_no);
        }else
            is_ok = set_fun(strip::DaqPkt::HK_SRC_DAQ,pol_no);

        if(! is_ok){
            std::stringstream ss;
            ss << "#" << _buffer[1] << "_" << message;
            send_back(ss.str(),true);
        }else{
            std::stringstream ss;
            ss << "#" << _buffer[1] << "_OK:"
               << std::hex << std::uppercase << pol_no << ";" << addr << ";"
               << values.size();
            send_back(ss.str(),true);
        }
    }

}

class HKmanager{
public:
    HKmanager(BoardMem* mem):
        _mem(mem),
        _mt(_rd()),
        _noise(0,50),
        _hk_src(DaqPkt::HK_SRC_BIAS),
        _table_id(0),
        _addr(0)
    {}

    uint8_t hksrc()   {return _hk_src;  }
    uint8_t table_id(){return _table_id;}
    uint8_t addr()    {return _addr;    }

    HKmanager& operator++(){
        _addr++;
        if(_addr >= MEM_CELLS){
            _addr = 0;
            _table_id++;
        }
        if(_table_id >7 && _table_id < 0xF){
            _table_id = 0xF;
        }else if(_table_id > 0xF){
            _table_id = 0;
            if(_hk_src == HK_SRC_BIAS){
                _hk_src = HK_SRC_DAQ;
            }else{
                shuffle();
                _hk_src = HK_SRC_BIAS;
            }
        }
        return *this;
    }

    HKvalue& get(){
        return _mem->mem[_hk_src][_table_id][_addr];
    }

private:
    void shuffle(){
        //std::cout << "shuffling..." << std::endl;
        //_noise(_mt)
        for (uint8_t pol=0; pol<8;pol++){
            for(uint8_t addr=0x50; addr<0x75; addr++){
                _mem->mem[HK_SRC_BIAS][pol][addr].u_val += 1;//_noise(_mt);
            }
        }
    }

    std::random_device _rd;
    std::mt19937 _mt;
    std::uniform_real_distribution<float> _noise;

    uint8_t _hk_src;
    uint8_t _table_id;
    uint8_t _addr;

    BoardMem* _mem;
};


void th_go(){
    strip::utils::ioService::service.run();
}

constexpr int th_num = 1;



int main(int argc, char const *argv[]) {

    try{
        strip::utils::parse_config("/etc/strip/config.json");
    }catch(const std::exception& e){
        std::cerr << e.what() <<std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }

    int64_t nano_sleep = 10000000;

    std::string server     = "localhost";
    std::string board_name = "";
    if(argc !=  3){
        std::cout << "usage: " << argv[0] << "<server_ip> <board name>" << std::endl;
        return -1;
    }else{
        server     = argv[1];
        board_name = argv[2];
    }

    const DaqConfig& conf = *singletons.config->daq_name.at(board_name);
    char board_id = conf.id;

    std::unique_ptr<SloDAQ> board;
    boost::asio::ip::address addr;
    addr = boost::asio::ip::address::from_string("0.0.0.0");//b.second->slo_ip
    std::cout << conf.slo_ip << ":" << conf.slo_port << std::endl;
    board.reset(new SloDAQ(addr,conf.slo_port,&board_memory));


    std::vector<std::thread> th;
    for (auto i=0; i<th_num; i++)
        th.push_back(std::thread(th_go));

    HKmanager hkm(&board_memory);
    /*setup DAQ env */

    udp::socket   socket(strip::utils::ioService::service, udp::endpoint(strip::utils::resolve_or_fail(conf.sci_ip), conf.sci_port));
    udp::endpoint remote_endpoint(strip::utils::resolve_or_fail(server),conf.sci_port);

    double_t ph = 0;
    double_t uph = 0;
    double_t p_step = 0.01;
    double_t u_step = 0.5;


    DaqUdpPkt pkts;
    for(auto& p : pkts.pkts)
        p.board_id = board_id;

    int pkt_count = 0;


    /*END setup DAQ env*/


    /* DATA LOOP*/
    while(true){
        auto t_begin = std::chrono::system_clock::now();

        for(size_t off=0; off < strip::DAQ_UDP_PKTS; off++){
            auto t_begin = std::chrono::system_clock::now();
            DaqPkt& p = pkts.pkts[off];
            gettimeofday(&p.timestamp,nullptr);
            p.phb = ((p.timestamp.tv_usec)/100)%2;

            int i = 1;
            for(auto j =0; j < NUM_POLS_PER_BOARD; j++){
                p.dem_Q1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_Q1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_U1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_U1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_U2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_U2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_Q2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_Q2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;
            }


            /* TODO HK*/
            p.hk.hk_src    = hkm.hksrc();
            p.hk.table_id  = hkm.table_id();
            p.hk.base_addr = hkm.addr();

            for(auto& h : p.hk.hk){
                h.u_val = hkm.get().u_val;
                ++hkm;
            }

            p.encode(&pkts.buffer[off * DAQ_PKT_LEN],&pkts.buffer[(off+1) * DAQ_PKT_LEN]);

            pkt_count++;
            ph += p_step;
            uph += u_step;

            auto stime = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - t_begin);
            std::this_thread::sleep_for(std::chrono::nanoseconds(nano_sleep - stime.count()));
        }
        socket.send_to(boost::asio::buffer(pkts.buffer),remote_endpoint);

    }


    for(auto& t : th)
        t.join();

    return 0;
}


/*
int main(int argc, char* argv[]){
    try{
        strip::utils::parse_config("/etc/strip/config.json");
    }catch(const std::exception& e){
        std::cerr << e.what() <<std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }



    std::vector<std::unique_ptr<SloDAQ>> boards;
    for(auto& b : strip::singletons.config->daq_id){
        boost::asio::ip::address addr;
        addr = boost::asio::ip::address::from_string("0.0.0.0");//b.second->slo_ip
        std::cout << b.second->slo_ip << ":" << b.second->slo_port << std::endl;
        std::unique_ptr<SloDAQ> ptr(new SloDAQ(addr,b.second->slo_port));
        boards.push_back(std::move(ptr));
    }

    try{
        if(argc == 2)
            delay = std::stoul(argv[1]);
    }catch(const std::exception& e){
        std::cout << e.what() << std::endl;
        std::cout << "usage: " << argv[0] << " [delay ms]" << std::endl;
        return -1;
    }

    std::vector<std::thread> th;
    for (auto i=0; i<th_num; i++)
        th.push_back(std::thread(th_go));

    for(auto& t : th)
        t.join();

    return 0;

}
*/
