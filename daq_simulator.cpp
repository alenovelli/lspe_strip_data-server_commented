#include "daq_pkt.hpp"
#include "strip_utils.hpp"
#include "slo_control.hpp"
#include "strip_singleton.hpp"
#include <thread>
#include <random>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <limits>

constexpr char BOARD_NO = 7;
constexpr uint16_t POL_NO = 8;

using namespace strip;
using boost::asio::ip::udp;

class HkDaq {
    typedef std::list<addr_item> list;
    typedef std::list<addr_item>::iterator iterator;
public:
    typedef std::array<uint16_t,4> element;

    HkDaq() : pol_no(0){
        for(list& l : hk)
            l = singletons.config->address_hk_pol;

        current = hk[0].begin();

    }

    element next(uint16_t d1, uint16_t d2){
        if(current == hk[pol_no].end()){
            pol_no +=1;
            pol_no %= POL_NO;
            current = hk[pol_no].begin();
        }
        element e;
        e[0] = pol_no;
        e[1] = current->address;
        e[2] = current->val;
        current->val += d1;
        ++current;

        if(current != hk[pol_no].end()){
            e[3] = current->val;
            current->val += d2;
            ++current;
        }else{
            e[3] = 0;
        }
        return e;
    }

private:
   list hk[POL_NO];
   uint8_t pol_no;
   iterator current;
};

int main(int argc, char* argv[]){
    try{
        strip::utils::parse_config("/etc/strip/config.json");
    }catch(const std::exception& e){
        std::cerr << e.what() <<std::endl;
        std::cerr << "EXIT due to a configuration error" << std::endl;
        return -1;
    }


    int64_t micro_sleep = 10000;
    std::string server = "localhost";
    if (argc > 1){
      server = argv[1];
    }
    if(argc == 3)
        micro_sleep = std::atoi(argv[2])*1000;
    if(argc >  3){
        std::cout << "usage: " << argv[0] << "<server_ip> [sampling_mills]" << std::endl;
        return -1;
    }

    uint8_t last_hk_addr = singletons.config->address_hk_pol.back().address;


    auto start = std::chrono::system_clock::now();

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<double> noise(0.0, 0.1);

    double_t ph = 0;
    double_t uph = 0;
    double_t p_step = 0.01;
    double_t u_step = 0.5;
    uint32_t last_timestamp = 0;


    HkDaq hk_pkts[BOARD_NO];

    udp::socket socket[BOARD_NO] = {
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service),
        udp::socket(strip::utils::ioService::service)
    };



    udp::endpoint remote_endpoint[BOARD_NO];

    udp::resolver resolver(strip::utils::ioService::service);

    for(auto i=0; i<BOARD_NO; i++){
        socket[i].open(udp::v4());

        udp::resolver::query query(udp::v4(),server, std::to_string(8000+i));
       remote_endpoint[i] = *resolver.resolve(query);
    }

    DaqUdpPkt pkts[BOARD_NO];
    for(char i=0; i<BOARD_NO; i++){
        for(auto& p : pkts[i].pkts)
            p.board_id = i + '0';
    }

    int pkt_count = 0;
    while(true){
        auto t_begin = std::chrono::system_clock::now();
        int p_off = pkt_count % DAQ_UDP_PKTS;

        if(pkt_count > 0 && p_off == 0){
            for(auto i=0; i<BOARD_NO; i++){
                socket[i].send_to(boost::asio::buffer(pkts[i].buffer),remote_endpoint[i]);
            }
        }

        if((pkt_count %500) == 0)
            std::cout<< std::setw(5) << std::setfill('0') <<last_timestamp << " TOT:" << pkt_count << std::endl;

        auto t = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start);

        for(DaqUdpPkt& u: pkts){
            DaqPkt& p = u.pkts[p_off];
            p.time_stamp = t.count()/10;
            p.phb = p.time_stamp%2;

            last_timestamp = p.time_stamp;

            int i = 1;
            for(auto j =0; j < NUM_POLS_PER_BOARD; j++){
                p.dem_Q1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_Q1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_U1(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_U1(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_U2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_U2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;

                p.dem_Q2(j) = std::cos(ph+20*i) * 200 + std::cos(uph)* 10;
                p.pwr_Q2(j) = std::cos(ph) * 100 * 1/i + std::cos(uph) * 10 + 120;
                i++;
            }
            HkDaq::element hk = hk_pkts[p.board_id - '0'].next(noise(mt)*100,noise(mt)*100);

            /*if(p.board_id=='0'){
                std::cout << hk[0] << std::hex << "[" << hk[1] << "]" << std::dec << std::endl;
            }*/

            p.pol_no       = hk[0];
            p.base_address = hk[1];
            p.hk_1         = hk[2];
            p.hk_2         = hk[3];

            p.encode(&u.buffer[p_off * DAQ_PKT_LEN],&u.buffer[(p_off+1) * DAQ_PKT_LEN]);
        }
        pkt_count++;
        ph += p_step;
        uph += u_step;

        auto stime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - t_begin).count();
        std::this_thread::sleep_for(std::chrono::microseconds(micro_sleep - stime));
    }

    return 0;
}
