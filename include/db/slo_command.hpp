#ifndef DB_SLO_COMMAND_HPP
#define DB_SLO_COMMAND_HPP

#include "../strip/utils.hpp"
#include <memory>
#include <vector>
#include <odb/mysql/database.hxx>
#include "../strip/slo_control.hpp"

namespace strip {

class DbSloCommand
{
public:
    DbSloCommand(const std::string &user, const std::string &password, const std::string &db_name, const std::string& host, uint16_t port);
    void connect_req(signal_slo_req& sig);
    void connect_ack(signal_slo_ack& sig);
    static const std::string class_name;
private:
    void slot_req(std::shared_ptr<const SloCommand> req){ persist(req,false); }
    void slot_ack(std::shared_ptr<const SloCommand> req){ persist(req,true); }
    void persist(std::shared_ptr<const SloCommand>& req, bool is_ack);

    std::unique_ptr<odb::core::database> _db;
    std::vector<signal_slo_req::Connection> _conn_req;
    std::vector<signal_slo_ack::Connection> _conn_ack;
};

}
#endif // DB_SLO_COMMAND_HPP
