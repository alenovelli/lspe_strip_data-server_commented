#ifndef DB_LOG_HPP
#define DB_LOG_HPP

#include "../strip/utils.hpp"
#include <memory>
#include <odb/mysql/database.hxx>

namespace strip {

class DbLog
{
public:
    DbLog(const std::string &user, const std::string &password, const std::string &db_name, const std::string& host, uint16_t port);
    void connect_log(utils::signal_log& sig);

    static const std::string class_name;
private:
    void slot_log(std::shared_ptr<const utils::LogMessage>);
    std::unique_ptr<odb::core::database> _db;
    utils::signal_log::Connection _conn_log;
};

}
#endif // DB_LOG_HPP
