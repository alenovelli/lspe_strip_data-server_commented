#ifndef DB_LOG_DATA_HPP
#define DB_LOG_DATA_HPP

#include <string>
#include <cstdint>

#pragma db object table("strip_log")
typedef struct {
public:
    enum Level {INFO=1,DEBUG=2,WARNING=4,ERROR=8};

#pragma db id auto
    uint64_t db_id;
    uint32_t level;
    std::string emit_class;
    std::string message;
    std::string user;
    uint64_t unix_nano;
    double mjd;
}DbLogMessage ;

#endif // DB_LOG_DATA_HPP
