#ifndef DB_SLO_COMMAND_DATA_HPP
#define DB_SLO_COMMAND_DATA_HPP

#include <string>
#include <cstdint>
#include <memory>
#include "../strip/slo_request_struct.hpp"

#pragma db object table("strip_slo")
typedef struct {
    enum  Ack {REQ,ACK};

    #pragma db id auto
    uint64_t db_id;

    std::string board_name;
    uint8_t pol_no;
    strip::SloCommand::Method method;
    strip::SloCommand::Type type;
    std::string base_addr;
    std::string message;
    std::string data;
    std::string user;
    int counter;
    Ack req_ack;
    #pragma db null
    std::unique_ptr<strip::SloCommand::Result> res;
    uint64_t unix_nano;
    double mjd;
}DbSloRequest;


#endif // DB_SLO_COMMAND_HPP
