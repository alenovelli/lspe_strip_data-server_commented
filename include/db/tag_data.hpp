#ifndef DB_TAG_DATA_HPP
#define DB_TAG_DATA_HPP

#include <cstdint>
#include <string>


#pragma db object table("strip_tag")
typedef struct{
#pragma db id auto
    uint64_t db_id;

    std::string tag;

    double mjd_start;
    std::string comment_start;

    double mjd_stop;
    std::string comment_stop;
}DbTag;

#endif // DB_TAG_DATA_HPP
