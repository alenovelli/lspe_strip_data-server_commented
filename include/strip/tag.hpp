#ifndef STRIP_TAG_HPP
#define STRIP_TAG_HPP
#include <string>
#include <memory>
#include "signals.hpp"
#include "tag_struct.hpp"
#include "../db/tag_data.hpp"
#include <odb/mysql/database.hxx>
#include <map>
#include <vector>

namespace strip {



typedef Signal<void(std::shared_ptr<const DbTag>)> signal_tag;


class TagControl{
public:
    enum class Status {OK,ALREADY_STARTED,UNKNOWN,SINTAX_ERROR};
    static const std::string class_name;

    TagControl(const std::string &user, const std::string &password, const std::string &db_name, const std::string& host, uint16_t port);
    signal_tag& sig_tag(){return _sig;}

    Status tag(Tag& t);

    std::vector<std::shared_ptr<const DbTag>> get_active();
private:
    signal_tag _sig;
    std::unique_ptr<odb::core::database> _db;
};

}

#endif // STRIP_TAG_HPP
