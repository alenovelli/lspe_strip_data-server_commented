#ifndef DAQ_FILE_STREAM_HPP
#define DAQ_FILE_STREAM_HPP
#include "file_stream.hpp"
#include <fstream>


namespace strip {

class DaqFileStream : public FileStream{
public:
    DaqFileStream();
protected:
    virtual void imp_flush();
    virtual void imp_add(std::shared_ptr<const DaqUdpPkt>&);
    virtual void imp_hk(std::shared_ptr<const msgpack::type::variant>& pkt){} //TODO?
    virtual bool imp_open(const std::string&);
    virtual void imp_close();
    virtual void imp_pol_flag(std::shared_ptr<const PolarimeterFlag>&){} //TODO
    virtual void imp_tel_flag(std::shared_ptr<const TelescopeFlag>&){}   //TODO
    virtual void imp_tag(std::shared_ptr<const DbTag>&){}   //TODO

    std::fstream _fs;
};

}
#endif // DAQ_FILE_STREAM_HPP
