namespace strip{
template <class Handler>
ServerTCP<Handler>::ServerTCP(uint16_t port)
    : _acceptor(strip::utils::ioService::service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
  _conn_close = utils::UnixSignals::halt3_ext().connect(std::bind(&ServerTCP<Handler>::slot_close, this));
  start_accept();
}

template <class Handler>
void ServerTCP<Handler>::start_accept()
{
  typename ConnectionSlaveTCP<Handler>::pointer new_connection =
      ConnectionSlaveTCP<Handler>::create(_acceptor.get_io_service(), new Handler());

  _acceptor.async_accept(new_connection->socket(),
                         boost::bind(&ServerTCP<Handler>::handle_accept, this, new_connection,
                                     boost::asio::placeholders::error));
}

template <class Handler>
void ServerTCP<Handler>::handle_accept(typename ConnectionSlaveTCP<Handler> ::pointer new_connection,
                              const boost::system::error_code &error)
{
  if (!error)
  {
    _conns.push_back(new_connection);
    new_connection->start_receive();
  }
  _conns.remove_if([](auto &c) -> bool { return !c->ok(); }); // remove dead connections
  start_accept();
}

template <class Handler>
int ServerTCP<Handler>::slot_close()
{
    _acceptor.close();
    for(auto& c : _conns){
        c->socket().cancel();
    }
    _conns.clear();
}


//////////////////////////////////////////////////////////////
template <class Handler>
typename ConnectionSlaveTCP<Handler>::pointer ConnectionSlaveTCP<Handler>::create(boost::asio::io_service &io_service, Handler *rh)
{
    return pointer(new ConnectionSlaveTCP(io_service, rh));
}

template <class Handler>
boost::asio::ip::tcp::socket &ConnectionSlaveTCP<Handler>::socket()
{
    return _socket;
}

template <class Handler>
bool ConnectionSlaveTCP<Handler>::ok() const
{
    return _ok;
}

template <class Handler>
void ConnectionSlaveTCP<Handler>::start_receive()
{

    memset(_buffer, '\0', BUFFER_SIZE);
    _socket.async_receive(boost::asio::buffer(_buffer),
                          boost::bind(&ConnectionSlaveTCP<Handler>::handle_read, boost::enable_shared_from_this<ConnectionSlaveTCP<Handler>>::shared_from_this(),
                                      boost::asio::placeholders::error,
                                      boost::asio::placeholders::bytes_transferred));
}

template <class Handler>
ConnectionSlaveTCP<Handler>::ConnectionSlaveTCP(boost::asio::io_service &io_service, Handler *hptr)
    : _socket(io_service), _handler(hptr), _ok(true)
{
}

template <class Handler>
void ConnectionSlaveTCP<Handler>::handle_read(const boost::system::error_code &ec,
                                     size_t s)
{
    if (ec)
    {
        _ok = false; // causes connection to terminate
    }
    else
    {
        std::string res = _handler->handle(std::string(_buffer));
        boost::system::error_code err;
        _socket.write_some(boost::asio::buffer(res), err);
        if (err)
            _ok = false;
    }
    if (_ok)
        start_receive();
    else // close connection and delete handler
    {
        _socket.close();
        _handler.reset();
    }
}


}