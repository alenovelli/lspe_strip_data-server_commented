#ifndef UTILS_HPP
#define UTILS_HPP

#include <utility>
#include <mutex>
#include <memory>
#include <chrono>
#include <boost/asio.hpp>
#include <erfa.h>
#include "signals.hpp"
#include "config.hpp"

namespace strip {
namespace utils {

typedef Signal<int()> signal_halt;

int init();
void connect_signals();

void parse_config(const std::string& path);

class ioService{
public:
    static boost::asio::io_service service;
};

class UnixSignals {
public:
    static signal_halt& halt3_ext();
    static signal_halt& halt2_ops();
    static signal_halt& halt1_db();
    static signal_halt& halt0_log();
    UnixSignals();
private:
    signal_halt _halt0;
    signal_halt _halt1;
    signal_halt _halt2;
    signal_halt _halt3;
};


typedef struct {
public:
    enum Level {INFO=1,DEBUG=2,WARNING=4,ERROR=8};

    Level level;
    std::string emit_class;
    std::string message;
    std::string user;
    std::chrono::system_clock::time_point timestamp;
}LogMessage ;

typedef Signal<void(std::shared_ptr<const LogMessage>)> signal_log;

class Log {
public:
    static void log(LogMessage::Level l,const std::string& emit_class, const std::string& message);
    static void log(LogMessage::Level l,
                    const std::string& emit_class,
                    const std::string& message,
                    const std::string& user);
    static signal_log& sig_log();
    static int init();
private:
    std::chrono::steady_clock::time_point _start;
    Log();
    static std::unique_ptr<Log> _singleton;
    signal_log  _sig;
};


class Time{
public:
    static void set_offset(uint32_t timestamp, timeval tv);
    static double get_MJD(timeval tv);
    static double get_pkt_MJD( uint32_t timestamp);
    static double get_unix_MJD( uint64_t timestamp_nano);
    static double get_now_MJD();
private:
    static timeval _start_tv;
    static uint32_t _start_pkt;
};

boost::asio::ip::address resolve_or_fail(const std::string& hostname);

}
}
#endif // UTILS_HPP
