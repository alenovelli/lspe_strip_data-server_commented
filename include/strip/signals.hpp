#ifndef STRIP_SIGNALS_HPP
#define STRIP_SIGNALS_HPP
#include "../redist/ThreadPool/ThreadPool.h"
#include <list>
#include <functional>
#include <tuple>
namespace strip {
template <typename T> class Signal;
template <class Ret, class... Args>
class Signal<Ret (Args...)> {
public:
    typedef typename std::shared_ptr<std::packaged_task<Ret()>> shared_task;
    struct connection_t {
        std::function<Ret(Args...)> task;
        std::deque<shared_task> queue;
        std::mutex queue_mutex;
        std::mutex work_mutex;
        size_t max_queue = 0; //DEBUG
    };

    typedef std::shared_ptr<connection_t> Connection;
    typedef typename std::deque<std::future<Ret>> deque_future;

    Signal(std::shared_ptr<ThreadPool> pool):_pool(pool){}


    Connection connect(std::function<Ret(Args...)> b){
        Connection c(new struct connection_t());
        c->task = b;
        std::unique_lock<std::mutex> lock(_list_mutex);
        _conn.push_back(c);
        return c;
    }

    deque_future operator()(Args... t){
        deque_future results;
        std::unique_lock<std::mutex> lock(_list_mutex);
        iterator it = _conn.begin();
        iterator end = _conn.end();

        while(it != end){
            Connection con = it->lock();
            iterator it2 = it++;
            if(con){
                shared_task stask = std::make_shared< std::packaged_task<Ret()> >(std::bind(con->task, t...));
                results.emplace_back(stask->get_future());
                {
                    std::unique_lock<std::mutex> qm(con->queue_mutex);
                    con->queue.emplace_back(std::move(stask));
                }
                _pool->enqueue([](std::weak_ptr<connection_t> l){
                    Connection c = l.lock();
                    if(c){
                        std::unique_lock<std::mutex> wm(c->work_mutex,std::try_to_lock);
                        if(!wm) //already locked by other thread, let it handle the queue
                            return;

                        while(true){
                            shared_task task;
                            {// queue_mutex scope
                                std::unique_lock<std::mutex> dm(c->queue_mutex);
                                c->max_queue = std::max(c->max_queue,c->queue.size());

                                if(c->queue.empty())
                                    break;

                                task = c->queue.front();
                                c->queue.pop_front();
                            }
                            (*task)();
                        }
                    } // if not c a broken promis will raise on waiters
                },*it2);

            }else{
                _conn.erase(it2);
            }
        }
        return results;
    }

    void disconnect_all_slots(){
        std::unique_lock<std::mutex> lock(_list_mutex);
        _conn.clear();
    }

private:
    typedef std::list<std::weak_ptr<connection_t>> list;
    typedef typename list::iterator iterator;
    list _conn;
    std::mutex _list_mutex;
    std::shared_ptr<ThreadPool> _pool;
};

}
#endif // STRIP_SIGNALS_HPP
