#ifndef STRIP_FLAGS_HPP
#define STRIP_FLAGS_HPP

#include "utils.hpp"

namespace strip {


 class PolarimeterFlag{
 public:
     enum class Flag : uint8_t {
         VALID   = 0,
         INVALID = 1,
     };

     PolarimeterFlag()=default;

     void dual(){
         uint8_t f = static_cast<uint8_t>(flag);
         f ^= -1;
         flag = static_cast<Flag>(f);
         clear = !clear;
     }

     uint8_t get() const{
         return static_cast<uint8_t>(flag);
     }

     Flag flag;
     std::string board_name;
     uint8_t pol_no;

     bool clear = false;
 };


 class TelescopeFlag{
 public:
     enum class Flag : uint32_t {
         VALID   = 0,
         INVALID = 1,
     };

     TelescopeFlag()=default;


     void dual(){
         uint32_t f = static_cast<uint32_t>(flag);
         f ^= -1;
         flag = static_cast<Flag>(f);
         clear = !clear;
     }

     uint32_t get() const{
         return static_cast<uint32_t>(flag);
     }

     Flag flag;
     bool clear = false;
 };


typedef Signal<void(std::shared_ptr<const PolarimeterFlag>) > signal_pol_flag;
typedef Signal<void(std::shared_ptr<const TelescopeFlag>) > signal_tel_flag;
}


#endif // STRIP_FLAGS_HPP
