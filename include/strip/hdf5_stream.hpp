#ifndef HDF5_STREAM_HPP
#define HDF5_STREAM_HPP
#include "file_stream.hpp"
#include "slo_control.hpp"
#include "utils.hpp"
#include "slo_request_struct.hpp"
#include <memory>
#include <vector>
#include <map>
#include <hdf5.h>
#include <H5Cpp.h>
#include <H5TBpublic.h>
#include <hdf5_hl.h>
#include <H5EnumType.h>
#include "../db/h5_info.hpp"
#include <future>


namespace strip {

typedef Signal<H5FileInfo(std::shared_ptr<const H5FileInfo>)> signal_hdf5_file;

class H5Stream: public FileStream{
public:
    static constexpr const size_t CHUNK_BYTE = 32768;

    typedef struct{
      double m_jd;
      int32_t dem_q1;
      int32_t dem_u1;
      int32_t dem_u2;
      int32_t dem_q2;
      uint32_t pwr_q1;
      uint32_t pwr_u1;
      uint32_t pwr_u2;
      uint32_t pwr_q2;
      uint8_t flags;
    }Pol_data;

    typedef struct{
        double m_jd;
        union {
            uint16_t u_val;
            int16_t  i_val;
        };
    }HK_data;

    typedef struct{
        double m_jd;
        double raw;
        double cal;
    }Cryo_data;

    typedef struct{
        double m_jd;
        double alt;
        double azi;
        double bor;
        uint32_t flags;
    }Tel_data;

    typedef struct{
        double m_jd;
        int64_t unix_time_s;
        int64_t unix_time_us;
        uint8_t  PHB;
    }Time_corr_data;

    typedef struct{
        uint64_t id;
        double m_jd_start;
        double m_jd_end;
        char tag[32];
        char start_comment[4096];
        char end_comment[4096];
    }Tag_data;

    typedef struct{
      utils::LogMessage::Level level;
      double                   m_jd;
      char                     emit_class[32];
      char                     user[32];
      char                     message[128];
    }Log_data;

    typedef struct {
      enum  Ack {REQ,ACK};

      char                      board_name[8];
      uint8_t                   pol_no;
      strip::SloCommand::Method method;
      strip::SloCommand::Type   type;
      char                      base_addr[16];
      char                      message[32];
      char                      data[32];
      char                      user[32];
      int                       counter;
      Ack                       req_ack;
      strip::SloCommand::Result res;
      double                    m_jd;
    }Command_data;

    typedef struct hk_t{
        //HK_data ref;
        std::vector<HK_data> data;
        hid_t gid;
        addr_item::Type type;

        std::string table_name;
        static constexpr const char* table_title = "Housekeeping data";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(HK_data);


        static constexpr const size_t dst_size =  sizeof(HK_data);
        static constexpr size_t num_fields = 2;
        static const char* field_names[num_fields];
        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields];// populated at runtime, depending on configured type

    }HK;

    typedef struct hk_key_t {
      char     board_id;
      std::string  table;
      uint8_t  pol;
      uint8_t  addr;

      bool operator<(const hk_key_t& other) const{
          if(board_id == other.board_id){
              if(table == other.table){
                  if(pol == other.pol){
                      return addr < other.addr;
                  }else{
                      return pol < other.pol;
                  }
              }else{
                  return table < other.table;
              }
          }else{
              return board_id < other.board_id;
          }
      }
    }HKkey;

    typedef struct cryo_t{
        std::vector<Cryo_data> data;
        hid_t gid;

        const std::string table_name = "cryo_data";
        static  constexpr const char* table_title = "Cryo probes data";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Cryo_data);

        static constexpr const size_t dst_size =  sizeof(Cryo_data);
        static constexpr size_t num_fields = 3;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields] = {
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_DOUBLE
        };
    }Cryo;

    typedef struct polarimeter_t{
        std::vector<Pol_data> data;
        //std::map<HKkey,HK> hk;
        uint8_t current_flag;
        hid_t gid;

        std::string group_name;

        const std::string table_name = "pol_data";
        static constexpr const char* table_title = "Polarimeter science data";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Pol_data);


        static constexpr const size_t dst_size =  sizeof(Pol_data);
        static constexpr size_t num_fields = 10;
        static const char* field_names[num_fields];
        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields] = {
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_INT32,
            H5T_NATIVE_UINT32,
            H5T_NATIVE_INT32,
            H5T_NATIVE_UINT32,
            H5T_NATIVE_INT32,
            H5T_NATIVE_UINT32,
            H5T_NATIVE_INT32,
            H5T_NATIVE_UINT32,
            H5T_NATIVE_UINT8
        };

        std::string board_name;
        std::string pol_name;

    }Polarimeter;

    typedef struct time_correlation_t{
        static constexpr char NULL_BOARD = 255;
        std::vector<Time_corr_data> data;
        uint8_t current_flag;
        hid_t gid;
        char reference_board = NULL_BOARD;

        const std::string table_name = "time_correlation";
        static  constexpr const char* table_title = "Instrument time and mdj correlation";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Time_corr_data);

        static constexpr const size_t dst_size =  sizeof(Time_corr_data);
        static constexpr size_t num_fields = 4;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields] = {
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_INT64,
            H5T_NATIVE_INT64,
            H5T_NATIVE_UINT8
        };
    }TimeCorrelation;

    typedef struct telescope_t{
        std::vector<Tel_data> data;
        uint8_t current_flag;
        hid_t gid;

        const std::string table_name = "tel_data";
        static  constexpr const char* table_title = "Telescope position data";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Tel_data);

        static constexpr const size_t dst_size =  sizeof(Tel_data);
        static constexpr size_t num_fields = 5;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields] = {
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_DOUBLE,
            H5T_NATIVE_UINT32
        };
    }Telescope;


    typedef struct tag_table_t{
        std::vector<Tag_data> data;
        uint8_t current_flag;
        hid_t gid;

        const std::string table_name = "tag_data";
        static  constexpr const char* table_title = "Tag intervals table";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Tag_data);

        static constexpr const size_t dst_size =  sizeof(Tag_data);
        static constexpr size_t num_fields = 6;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields];

        tag_table_t(){
            hid_t string_tag = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_tag, sizeof(Tag_data::tag) );

            hid_t string_start_comment = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_start_comment, sizeof(Tag_data::start_comment) );

            hid_t string_end_comment = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_end_comment, sizeof(Tag_data::end_comment) );

            field_type[0] = H5T_NATIVE_UINT64;
            field_type[1] = H5T_NATIVE_DOUBLE;
            field_type[2] = H5T_NATIVE_DOUBLE;
            field_type[3] = string_tag;
            field_type[4] = string_start_comment;
            field_type[5] = string_end_comment;
        }
    }TagTable;

    typedef struct log_table_t{
        std::vector<Log_data> data;
        uint8_t current_flag;
        hid_t gid;

        const std::string table_name = "log_data";
        static  constexpr const char* table_title = "Data serve log table";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Log_data);

        static constexpr const size_t dst_size =  sizeof(Log_data);
        static constexpr size_t num_fields = 5;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields];

        H5::EnumType level_enum;

        log_table_t():level_enum(sizeof(utils::LogMessage::Level)){
/*
      utils::LogMessage::Level level;
      double                   m_jd;
      char                     emit_class[32];
      char                     user[32];
      char                     message[128];
*/
            utils::LogMessage::Level l = utils::LogMessage::Level::DEBUG;
            level_enum.insert("DEBUG",&l);

            l = utils::LogMessage::Level::INFO;
            level_enum.insert("INFO",&l);

            l = utils::LogMessage::Level::WARNING;
            level_enum.insert("WARNING",&l);

            l = utils::LogMessage::Level::ERROR;
            level_enum.insert("ERROR",&l);

            hid_t string_emit_class = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_emit_class, sizeof(Log_data::emit_class) );

            hid_t string_user = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_user, sizeof(Log_data::user) );

            hid_t string_message = H5Tcopy( H5T_C_S1 );
            H5Tset_size( string_message, sizeof(Log_data::message) );

            field_type[0] = level_enum.getId();
            field_type[1] = H5T_NATIVE_DOUBLE;
            field_type[2] = string_emit_class;
            field_type[3] = string_user;
            field_type[4] = string_message;
        }
    }LogTable;

    typedef struct command_table_t{
      /*
      char                      board_name[8];
      uint8_t                   pol_no;
      strip::SloCommand::Method method;
      strip::SloCommand::Type   type;
      char                      base_addr[16];
      char                      message[32];
      char                      data[32];
      char                      user[32];
      int                       counter;
      Ack                       req_ack;
      strip::SloCommand::Result res;
      double                    m_jd;
      */
        std::vector<Command_data> data;
        uint8_t current_flag;
        hid_t gid;

        const std::string table_name = "commands_data";
        static  constexpr const char* table_title = "SLO commads table";
        static constexpr const int chunk = CHUNK_BYTE / sizeof(Command_data);

        static constexpr const size_t dst_size =  sizeof(Command_data);
        static constexpr size_t num_fields = 12;
        static const char* field_names[num_fields];

        static const size_t dst_offset[num_fields];

        static const size_t dst_sizes[num_fields];

        hid_t field_type[num_fields];

        H5::EnumType enum_method;
        H5::EnumType enum_type;
        H5::EnumType enum_ack;
        H5::EnumType enum_res;

        command_table_t() :
          enum_method(sizeof(strip::SloCommand::Method)),
          enum_type(sizeof(strip::SloCommand::Type)),
          enum_ack(sizeof(Command_data::Ack)),
          enum_res(sizeof(strip::SloCommand::Result))
        {
            SloCommand::Method m = SloCommand::Method::NONE;
            enum_method.insert("NONE",&m);

            m = SloCommand::Method::SET;
            enum_method.insert("SET",&m);

            m = SloCommand::Method::GET;
            enum_method.insert("GET",&m);


            SloCommand::Type t = SloCommand::Type::NONE;
            enum_type.insert("NONE",&t);

            t = SloCommand::Type::BIAS;
            enum_type.insert("BIAS",&t);

            t = SloCommand::Type::DAQ;
            enum_type.insert("DAQ",&t);

            t = SloCommand::Type::CRYO;
            enum_type.insert("CRYO",&t);


            Command_data::Ack a = Command_data::Ack::REQ;
            enum_ack.insert("REQ",&a);

            a = Command_data::Ack::ACK;
            enum_ack.insert("ACK",&a);


            SloCommand::Result r = SloCommand::Result::OK;
            enum_res.insert("OK",&r);

            r = SloCommand::Result::ERROR;
            enum_res.insert("ERROR",&r);

            r = SloCommand::Result::UNKNOWN_COMMAND;
            enum_res.insert("UNKNOWN_COMMAND",&r);

            r = SloCommand::Result::ERROR_RO;
            enum_res.insert("ERROR_RO",&r);

            r = SloCommand::Result::BAD_ADDRESS;
            enum_res.insert("BAD_ADDRESS",&r);

            r = SloCommand::Result::BAD_TYPE;
            enum_res.insert("BAD_TYPE",&r);

            r = SloCommand::Result::ERROR_SYNTAX;
            enum_res.insert("ERROR_SYNTAX",&r);

            r = SloCommand::Result::ERROR_COMM;
            enum_res.insert("ERROR_COMM",&r);

            r = SloCommand::Result::ERROR_DECODE;
            enum_res.insert("ERROR_DECODE",&r);

            r = SloCommand::Result::ERROR_DATA;
            enum_res.insert("ERROR_DATA",&r);


            field_type[0]  = h5_string_type(sizeof(Command_data::board_name));
            field_type[1]  = H5T_NATIVE_UINT8;
            field_type[2]  = enum_method.getId();
            field_type[3]  = enum_type.getId();
            field_type[4]  = h5_string_type(sizeof(Command_data::base_addr));
            field_type[5]  = h5_string_type(sizeof(Command_data::message));
            field_type[6]  = h5_string_type(sizeof(Command_data::data));
            field_type[7]  = h5_string_type(sizeof(Command_data::user));
            field_type[8]  = H5T_NATIVE_INT32;
            field_type[9]  = enum_ack.getId();
            field_type[10] = enum_res.getId();
            field_type[11] = H5T_NATIVE_DOUBLE;
        }
    }CommandTable;



public:
    typedef Signal<void(std::shared_ptr<const msgpack::type::variant>)> signal_msgpack;
    H5Stream();
    signal_hdf5_file& sig_file(){return _sig_file;}
    void connect_log(utils::signal_log& sig);
    void connect_req(signal_slo_req& sig);
    void connect_ack(signal_slo_ack& sig);
    void connect_cryo(signal_msgpack& sig);
protected:
    virtual void imp_flush();
    virtual void imp_add(std::shared_ptr<const DaqUdpPkt>&);
    virtual void imp_hk(std::shared_ptr<const msgpack::type::variant>&);
    virtual bool imp_open(const std::string&);
    virtual void imp_close();
    virtual void imp_pol_flag(std::shared_ptr<const PolarimeterFlag>&);
    virtual void imp_tel_flag(std::shared_ptr<const TelescopeFlag>&);
    virtual void imp_tag(std::shared_ptr<const DbTag>&);
private:
    static hid_t h5_string_type(size_t size){
        hid_t type_id = H5Tcopy( H5T_C_S1 );
        H5Tset_size(type_id , size);
        return type_id;
    }
    void slot_log(std::shared_ptr<const utils::LogMessage>);
    void slot_cryo(std::shared_ptr<const msgpack::type::variant>);
    void slot_req(std::shared_ptr<const SloCommand> req){ do_slot_command(req,false); }
    void slot_ack(std::shared_ptr<const SloCommand> req){ do_slot_command(req,true); }
    void do_slot_command(std::shared_ptr<const SloCommand>& req, bool is_ack);
    void write_final_attributes(H5FileInfo &info);
    void finalize_tags();

//    inline void update_hk(const DaqPkt& pkt);

    std::map<char,std::vector<Polarimeter>> _pols;

    //            board      ->        pol       ->     table        ->     address -> value
    std::map<std::string,std::map<std::string,std::map<std::string,std::map<std::string,HK>>>> _hk;

    std::map<std::string,Cryo> _cryo;
//    std::map<HKkey,HK> _hk;
    TimeCorrelation _time_corr;
    Telescope       _tel;
    TagTable        _tag_table;
    LogTable        _log_table;
    CommandTable    _command_table;

//    std::map<HKkey,HKitem> _hk_snapshot;

    signal_hdf5_file _sig_file;

    std::map<std::string,Tag_data> _tags;

    std::unique_ptr<H5::H5File> _file;
    std::vector<H5::Group> _groups;

    std::future<H5FileInfo> _db_info;

    utils::signal_log::Connection _conn_log;
    std::vector<signal_slo_req::Connection> _conn_req;
    std::vector<signal_slo_ack::Connection> _conn_ack;

    signal_msgpack::Connection _conn_cryo;
};

}
#endif // HDF5_STREAM_HPP
