#ifndef STRIP_TAG_STRUCTURE_HPP
#define STRIP_TAG_STRUCTURE_HPP
#include <string>

namespace strip {
typedef struct{
    enum class Type { START, STOP};
    std::string tag;
    Type type;
    std::string comment;
    double mjd;
}Tag;
}
#endif // STRIP_TAG_STRUCTURE_HPP
