#ifndef AUTOBAHN_SNAPSHOT_HPP
#define AUTOBAHN_SNAPSHOT_HPP
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include "../strip/daq_pkt.hpp"
#include "base.hpp"
#include "../strip/utils.hpp"
#include <boost/variant.hpp>

namespace strip {
namespace autobahn{


namespace _internal {
    typedef boost::variant<uint16_t,int16_t,float> value;

    typedef struct value_pod_t{
        static constexpr size_t BUFF = 32;
        char board[BUFF];
        char pol[BUFF];
        char table[BUFF];
        char addr[BUFF];
        value val;

        value_pod_t(){
            std::memset(board,'\0',BUFF);
            std::memset(pol,  '\0',BUFF);
            std::memset(table,'\0',BUFF);
            std::memset(addr, '\0',BUFF);
        }
    } value_pod;
}


class Snapshot{
public:
    typedef _internal::value value;
    typedef _internal::value_pod value_pod;

    typedef std::map<std::string,value> addr_map;

    typedef std::map<std::string,value_pod*> addr_map_ptr;
    typedef std::map<std::string,addr_map_ptr> table_map;
    typedef std::map<std::string,table_map> pol_map;
    typedef std::map<std::string,pol_map> snapshot_map;

    typedef std::map<std::string,std::map<std::string,std::map<std::string,std::vector<std::string>>>>  arg_map;
    typedef std::map<std::string,std::map<std::string,std::map<std::string,std::map<std::string,value>>>> result_map;

    typedef boost::interprocess::allocator<value_pod, boost::interprocess::managed_shared_memory::segment_manager>
       sh_allocator;

    typedef std::vector<value_pod,sh_allocator> sh_vector;

    Snapshot();
    ~Snapshot();

    void connect_pkt(signal_pkt& sig);
    static const std::string class_name;
    static const std::string sh_mem_name;
    static const std::string sh_vec_name;
private:
    void slot_pkt(std::shared_ptr<const DaqUdpPkt>);
    int  slot_close();

    void update(const addr_map &src, addr_map_ptr& dst);

    void snapshot(::autobahn::wamp_invocation invocation);

    std::vector<signal_pkt::Connection> _conn_pkt;
    utils::signal_halt::Connection _conn_close;

    snapshot_map _snapshot;
    float _alpha;
    boost::interprocess::managed_shared_memory _segment;
    std::unique_ptr<sh_vector> _sh_vec;

    ::autobahn::wamp_registration _registration;
};
}
}
#endif // STRIP_SNAPSHOT_HPP
