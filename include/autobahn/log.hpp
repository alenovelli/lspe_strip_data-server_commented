#ifndef AUTOBAHN_LOG_HPP
#define AUTOBAHN_LOG_HPP
#include "base.hpp"
#include "../strip/utils.hpp"

namespace strip{
namespace autobahn{

class Log : public Base{
public:
    Log(const boost::asio::ip::address& addr,uint16_t port);
    void connect_log(utils::signal_log& sig);

    static const std::string class_name;
private:
    void slot_log(std::shared_ptr<const utils::LogMessage>);
    int  slot_close();

    utils::signal_log::Connection _conn_log;
    utils::signal_halt::Connection _conn_close;
};

}
}

#endif // AUTOBAHN_LOG_HPP
