#ifndef LAKESHORE_HPP
#define LAKESHORE_HPP

#include "cryo_unit.hpp"
#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <mutex>


class Lakeshore : public CryoUnit
{
public:
    
    Lakeshore();
    virtual command_t send_command(const command_t &cmd) override;
    
protected:
    virtual void do_send(const std::stringstream &msg, boost::system::error_code& ec) =0;
    virtual std::stringstream do_receive(uint64_t ms_timeout, boost::system::error_code& ec)=0;

    bool encode(command_t &cmd, std::stringstream &ss);
    bool decode(std::stringstream &buff, command_t &cmd);
    const std::string term() const { return "\r\n"; }
private:
    bool has_return(const command_t &cmd);
    std::mutex _cmd_sync;
};

#endif // LAKESHORE_HPP
