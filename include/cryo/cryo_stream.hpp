#pragma once
#define MSGPACK_USE_BOOST 1
#include <boost/variant.hpp>
#include <msgpack.hpp>
#include "../strip/signals.hpp"
#include "../strip/timer.hpp"

namespace strip
{

    typedef Signal<void(std::shared_ptr<const msgpack::type::variant>)> signal_cryo_pkt;

    class CryoStream
    {
    public:
        CryoStream(uint32_t _multiplexer_hysteresis);
        void connect(signal_timer &sig);
        signal_cryo_pkt &sig_cryo_pkt() { return _cryo_pkt_sig; }

    private:
        void slot_collect_cryo();
        signal_cryo_pkt _cryo_pkt_sig;

        uint32_t _multiplexer_hysteresis;

        signal_timer::Connection _conn;
    };
} // namespace strip
