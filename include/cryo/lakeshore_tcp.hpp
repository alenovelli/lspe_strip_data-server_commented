#ifndef LAKESHORE_TCP_HPP
#define LAKESHORE_TCP_HPP
#include "lakeshore.hpp"
#include <boost/asio.hpp>

class LakeshoreTCP : public Lakeshore
{
    static constexpr uint64_t BUFFER_SIZE  = 4096;
public:
    LakeshoreTCP();
    virtual void set_config(const boost::property_tree::ptree &pt);
    virtual void start(boost::system::error_code& ec);
    virtual void stop(boost::system::error_code& ec);

protected:
    virtual void do_send(const std::stringstream &msg, boost::system::error_code& ec);
    virtual std::stringstream do_receive(uint64_t ms_timeout, boost::system::error_code& ec);

private:
    void connect(boost::system::error_code& ec);
    boost::asio::ip::tcp::socket _socket;
    std::string _ip;
    uint16_t _port;
    char _buffer[BUFFER_SIZE];
};

#endif