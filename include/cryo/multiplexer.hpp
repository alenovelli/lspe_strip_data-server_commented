#ifndef MULTIPLEXER_HPP
#define MULTIPLEXER_HPP

#include "cryo_unit.hpp"
#include <boost/asio.hpp>

class Multiplexer : public CryoUnit
{
        static constexpr uint64_t BUFFER_SIZE = 4096;
    static const size_t MAX_TIMEOUTS = 5;
    static const size_t WAIT_MILLIS = 500;

public:
    Multiplexer();
    virtual command_t send_command(const command_t &cmd) override;

    virtual void set_config(const boost::property_tree::ptree &pt) override;
    virtual void start(boost::system::error_code &ec) override;
    virtual void stop(boost::system::error_code &ec) override;
    static const std::string class_name;

private:
    std::string _ip;
    uint16_t _port;
    boost::asio::ip::udp::endpoint _remote_endpoint;
    boost::asio::ip::udp::socket _socket;

    size_t _n_timeouts; // after MAX_TIMEOUTS we refresh the connection
        char _buffer[BUFFER_SIZE];
};

#endif