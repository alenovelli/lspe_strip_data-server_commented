#ifndef CRYO_UNIT_HPP
#define CRYO_UNIT_HPP

#define MSGPACK_USE_BOOST 1
#include <msgpack.hpp>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/asio.hpp>
#include <string>
#include <vector>
#include <map>

class CryoUnit{
public:
    typedef msgpack::type::variant command_t;

    virtual command_t send_command(const command_t &cmd) = 0;

    virtual void set_config(const boost::property_tree::ptree &pt);
    virtual void start(boost::system::error_code& ec) = 0;
    virtual void stop(boost::system::error_code& ec) = 0;

    protected:
    std::map<std::string, std::pair<std::vector<std::string>, std::vector<std::string>>> _commands;
};

#endif // CRYO_UNIT_HPP