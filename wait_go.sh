#!/bin/sh

cmd="$@"

echo "*** LAUNCH WAIT PROGRAM ***"

python3 wait4wamp.py

echo "*** LAUNCH MAIN PROGRAM ***"

exec $cmd
